function(add_cyclux_test target)
  add_executable(${target}_test ${target}.cc)
  target_link_libraries(${target}_test AST Lex Tooling gtest gtest_main)
  add_test(NAME ${target} COMMAND ${target}_test)
endfunction()