function(add_cyclux_tool target)
  add_executable(${target} ${target}.cc)
  target_link_libraries(${target} AST Lex Tooling)
endfunction()