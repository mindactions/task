# Grammar


```
expression ::= 
    assignment-expression
    expression, assignment-expression

assignment-expression ::= 
    conditional-expression
    unary-expression assignment-operator assignment-expression

 assignment-operator ::=
    = | -= | += | *= | /=

conditional-expression ::=
    logical-or-expression

logical-or-expression ::=
    logical-and-expression
    logical-or-expression || logical-and-expression

logical-and-expression ::=
    equality-expression
    logical-and-expression && equality-expression

equality-expression ::=
    relational-expression
    relational-expression == relational-expression
    relational-expression != relational-expression

relational-expression ::=
    additive-expression
    additive-expression < additive-expression
    additive-expression > additive-expression
    additive-expression <= additive-expression
    additive-expression >= additive-expression

additive-expression ::=
    multiplicative-expression
    additive-expression + multiplicative-expression
    additive-expression - multiplicative-expression

multiplicative-expression ::=
    unary-expression
    muliplicative-expression * unary-expression
    muliplicative-expression / unary-expression
    muliplicative-expression % unary-expression

unary-expression ::=
    postfix-expression
    ++ unary-expression
    -- unary-expression
    ! unary-expression

postfix-expression ::=
    primary-expression
    postfix-expression ++
    postfix-expression --

primary-expression ::=
    literal
    name
    ( assign-expression )


statements ::=
    statement
    statements statement

statement ::=
    expression-statement
    declaration-statement
    selection-statement
    iteration-statement
    jump-statement
    compound-statement

expression-statement ::=
    expression ;

declaration-statement ::=
    declaration

selection-statement ::=
    if ( expression ) statement
    if ( expression ) statement else statement

iteration-statement ::=
    while ( expression ) statement
    do statement while ( expression );
    for (for-init-statement expression ; expression) statement

for-init-statement ::=
    expression-statement
    declaration-statement

jump-statement ::=
    break ;
    continue ;

compound-statement ::=
    { statement-list }

statement-list ::=
    statement
    statement-list statement


declaration ::=
    decl-simple-type declarator-list ;

decl-simple-type ::=
    int

declarator-list ::=
    init-declarator
    declarator-list , init-declarator

init-declarator ::=
    declarator initializer

declarator ::=
    name

initializer ::=
    = expression
```
