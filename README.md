# FTW?


## Build

Linux:

```sh
$ mkdir build && cd build
$ cmake ..
$ make
```

Windows:

Install CMake. Open CMake GUI. Set source and build directory. Open solution file in the `build`
directory.

