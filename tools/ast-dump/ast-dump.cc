#include "Tooling/Tool.h"
#include "Tooling/ASTConsumer.h"
#include "AST/Stmt.h"
#include <iostream>
#include <memory>


using std::cout;
using std::endl;


class DumpASTConsumer : public ASTConsumer
{
public:
  void RunASTConsumer(ASTContext &ctx)
  {
    cout << ctx.GetRoot()->ToString() << endl;
  }
};


int main(int argc, char **argv)
{
  Tool tool(argc, argv);
  std::unique_ptr<DumpASTConsumer> consumer(new DumpASTConsumer());
  return tool.run(std::move(consumer));
}