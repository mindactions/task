#include "Tooling/Tool.h"
#include "Tooling/ASTConsumer.h"
#include "AST/Stmt.h"
#include "AST/Scope.h"
#include <iostream>
#include <memory>
#include <map>
#include <sstream>
#include <vector>
#include <list>
#include <algorithm>


using std::cout;
using std::endl;


const int MAX_LOOP_ITERATIONS = 1000000;
const int POSSIBLE_INFINITE_LOOP = -1;
const int INSIDE_INFINITE_LOOP = -2;


//  -=============================-
//
//  Core data structures
//
//  -=============================-

enum LoopType {
  None   = 0,
  Do        = 1,
  While     = 2,
  For       = 3
};


class LoopStack
{
public:
  void Enter() { m_loopStack.push_back(false); }
  void Leave() { m_loopStack.pop_back(); }

  void SetBreak(bool hasBreak) { m_loopStack.back() = hasBreak; }
  bool GetBreak() { return m_loopStack.back(); }

private:
  std::list<bool> m_loopStack;
};


struct Loop {
  Loop(LoopType type = LoopType::None,
       SourceLocation loc = SourceLocation(),
       int exitPoints = 0,
       int level = 0,
       int iterations = 0) :
      m_type(type),
      m_location(loc),
      m_exitPoints(exitPoints),
      m_level(level),
      m_iterations(iterations)
  {
  }

  LoopType m_type;
  SourceLocation m_location;
  int m_exitPoints;
  int m_level;
  int m_iterations;

  std::string ToString() const
  {
    std::string type = "";
    switch (m_type)
    {
      case LoopType::Do:
        type = "Do";
        break;
      case LoopType::While:
        type = "While";
        break;
      case LoopType::For:
        type = "For";
        break;
      default:
      break;
    }

    std::stringstream sout;
    sout << "<";
    sout << type << " ";
    sout << m_location;
    sout << ", exit points: " << m_exitPoints;
    sout << ", level: " << m_level;
    sout << ", iterations: " << m_iterations;
    sout << ">";
    return sout.str();
  }
};


class LoopMetadata {
public:
  LoopMetadata() {}
  ~LoopMetadata() {}

  int Size() { return m_loops.size(); }
  Loop &At(Stmt *stmt) { return m_loops[stmt]; }
  void IncrementExitPoints(Stmt *stmt) { At(stmt).m_exitPoints++; }
  void IncrementIterations(Stmt *stmt) { At(stmt).m_iterations++; }
  void SetLevel(Stmt *stmt, int level) { At(stmt).m_level = level; }

  std::map<Stmt*, Loop>::iterator Begin() { return m_loops.begin(); }
  std::map<Stmt*, Loop>::iterator End() { return m_loops.end(); }
  std::map<Stmt*, Loop>::const_iterator Begin() const { return m_loops.begin(); }
  std::map<Stmt*, Loop>::const_iterator End() const { return m_loops.end(); }

  std::vector<Loop> ToVector()
  {
    std::vector<Loop> loops;
    for (auto i = m_loops.begin(); i != m_loops.end(); i++) loops.push_back(i->second);
    return loops;
  }

  std::string ToString() const
  {
    std::stringstream sout;
    for (auto i = m_loops.begin(); i != m_loops.end(); i++)
      sout << i->second.ToString() << std::endl;
    return sout.str();
  }

private:
  std::map<Stmt*, Loop> m_loops;
};


//  -=============================-
//
//  First pass
//  Initialize LoopMetadata
//
//  -=============================-
class InitializePass : public RecursiveASTVisitor
{
public:
  InitializePass(ASTContext *context, LoopMetadata *loops) :
      RecursiveASTVisitor(context), m_loops(loops)
  {
  }

  bool VisitForStmt(ForStmt *stmt)
  {
    Loop &loop = m_loops->At(stmt);
    loop.m_type = LoopType::For;
    loop.m_location = stmt->GetLocation();
    return true;
  }

  bool VisitDoStmt(DoStmt *stmt)
  {
    Loop &loop = m_loops->At(stmt);
    loop.m_type = LoopType::Do;
    loop.m_location = stmt->GetLocation();
    return true;
  }

  bool VisitWhileStmt(WhileStmt *stmt)
  {
    Loop &loop = m_loops->At(stmt);
    loop.m_type = LoopType::While;
    loop.m_location = stmt->GetLocation();
    return true;
  }

private:
  LoopMetadata *m_loops;
};


//  -=============================-
//
//  Second pass
//  Fill level field
//
//  -=============================-
class LevelPass : public RecursiveASTVisitor
{
public:
  LevelPass(ASTContext *context, LoopMetadata *loops) :
      RecursiveASTVisitor(context), m_loops(loops)
  {
  }

  void BeforeTraverse() override { m_level = 0; }

  bool VisitForStmt(ForStmt *stmt)
  {
    m_loops->SetLevel(stmt, m_level);
    m_level++;
    stmt->GetBody()->Accept(this);
    m_level--;
    return false;
  }

  bool VisitDoStmt(DoStmt *stmt)
  {
    m_loops->SetLevel(stmt, m_level);
    m_level++;
    stmt->GetBody()->Accept(this);
    m_level--;
    return false;
  }

  bool VisitWhileStmt(WhileStmt *stmt)
  {
    m_loops->SetLevel(stmt, m_level);
    m_level++;
    stmt->GetBody()->Accept(this);
    m_level--;
    return false;
  }

private:
  LoopMetadata *m_loops;
  int m_level;
};


//  -=============================-
//
//  Third pass
//  Fill exit points field
//
//  -=============================-
class ExitPointsPass : public RecursiveASTVisitor
{
public:
  ExitPointsPass(ASTContext *context, LoopMetadata *loops) :
      RecursiveASTVisitor(context), m_loops(loops)
  {
  }

  bool VisitForStmt(ForStmt *stmt)
  {
    m_currentLoop = stmt;
    return true;
  }

  bool VisitDoStmt(DoStmt *stmt)
  {
    m_currentLoop = stmt;
    return true;
  }

  bool VisitWhileStmt(WhileStmt *stmt)
  {
    m_currentLoop = stmt;
    return true;
  }

  bool VisitBreakStmt(BreakStmt *stmt)
  {
    m_loops->IncrementExitPoints(m_currentLoop);
    return true;
  }

private:
  LoopMetadata *m_loops;
  Stmt *m_currentLoop;
};


//  -=============================-
//
//  Forth pass
//  Fill iterations field
//
//  -=============================-
class IterationsPass : public RecursiveASTVisitor
{
public:
  IterationsPass(ASTContext *context, LoopMetadata *loops) :
      RecursiveASTVisitor(context), m_loops(loops)
  {
  }

  void BeforeTraverse() override { m_scope.Enter(); }
  void AfterTraverse() override { m_scope.Leave(); }

  bool VisitVarDecl(VarDecl *decl) override
  {
    VarExpr *lhs = decl->GetLHS();
    int rhs = Eval(decl->GetRHS());
    m_scope.Declare(lhs->GetIdentifier(), rhs);
    return false;
  }

  bool VisitForStmt(ForStmt *stmt) override
  {
	  Loop &forLoop = m_loops->At(stmt);
    if (forLoop.m_iterations < 0) return false;
    m_scope.Enter();
    m_loopStack.Enter();
    stmt->GetInit()->Accept(this);
    while (true)
    {
      int result = Eval(stmt->GetCond());
      if (!result)
        break;
      m_loopStack.SetBreak(false);
      stmt->GetBody()->Accept(this);
      if (m_loopStack.GetBreak()) break;
      Eval(stmt->GetStep());
      forLoop.m_iterations++;

      // prevent infinite loop
      if (forLoop.m_iterations > MAX_LOOP_ITERATIONS)
      {
        forLoop.m_iterations = POSSIBLE_INFINITE_LOOP;
        break;
      }
    }
    m_loopStack.Leave();
    m_scope.Leave();
    return false;
  }

  bool VisitDoStmt(DoStmt *stmt) override
  {
    Loop &doLoop = m_loops->At(stmt);
    if (doLoop.m_iterations < 0) return false;
    m_scope.Enter();
    m_loopStack.Enter();
    int cond = 0;
    do
    {
      if (doLoop.m_iterations > MAX_LOOP_ITERATIONS)
      {
        doLoop.m_iterations = POSSIBLE_INFINITE_LOOP;
        break;
      }

      m_loopStack.SetBreak(false);
      stmt->GetBody()->Accept(this);
      if (m_loopStack.GetBreak()) break;
      doLoop.m_iterations++;
      cond = Eval(stmt->GetCond());
    } while (cond);
    m_loopStack.Leave();
    m_scope.Leave();
    return false;
  }

  bool VisitWhileStmt(WhileStmt *stmt) override
  {
    Loop &whileLoop = m_loops->At(stmt);
    if (whileLoop.m_iterations < 0) return false;
    m_scope.Enter();
    m_loopStack.Enter();
    int cond = 0;
    while (cond = Eval(stmt->GetCond()))
    {
      m_loopStack.SetBreak(false);
      stmt->GetBody()->Accept(this);
      if (m_loopStack.GetBreak()) break;
      whileLoop.m_iterations++;

      if (whileLoop.m_iterations > MAX_LOOP_ITERATIONS)
      {
        whileLoop.m_iterations = POSSIBLE_INFINITE_LOOP;
        break;
      }
    }
    m_loopStack.Leave();
    m_scope.Leave();
    return false;
  }

  bool VisitExpressionStmt(ExpressionStmt *stmt)
  {
    if (stmt->GetExpr()) Eval(stmt->GetExpr());
    return false;
  }

  bool VisitBreakStmt(BreakStmt *stmt) override
  {
    m_loopStack.SetBreak(true);
    return false;
  }

  bool VisitIfStmt(IfStmt *stmt) override
  {
    int cond = Eval(stmt->GetCond());
    if (cond)
      stmt->GetThen()->Accept(this);
    else if (stmt->GetElse())
      stmt->GetElse()->Accept(this);

    return false;
  }

  int Eval(Expr *expr)
  {
    if (isa<NumberExpr>(expr)) return EvalNumber(dyn_cast<NumberExpr>(expr));
    else if (isa<VarExpr>(expr)) return EvalVar(dyn_cast<VarExpr>(expr));
    else if (isa<BinOpExpr>(expr)) return EvalBinary(dyn_cast<BinOpExpr>(expr));
    else if (isa<UnaryExpr>(expr)) return EvalUnary(dyn_cast<UnaryExpr>(expr));
    else if (isa<ConsExpr>(expr)) return EvalCons(dyn_cast<ConsExpr>(expr));

    return 0;
  }

  int EvalUnary(UnaryExpr *expr)
  {
    int e = Eval(expr->GetExpr());
    int result = 0;

    switch (expr->GetOp())
    {
      case UnaryOp::INC:
        result = ++e;
        break;
      case UnaryOp::DEC:
        result = --e;
        break;
      case UnaryOp::NOT:
        result = !e;
        break;
      default:
        break;
    }

    VarExpr *var = dyn_cast<VarExpr>(expr->GetExpr());
    m_scope.Set(var->GetIdentifier(), result);
    return result;
  }

  int EvalBinary(BinOpExpr *expr)
  {
    if (expr->GetOp() == BinOp::MOV)
    {
      VarExpr *lhs = dyn_cast<VarExpr>(expr->GetLeft());
      int rhs = Eval(expr->GetRight());
      m_scope.Set(lhs->GetIdentifier(), rhs);
      return rhs;
    }

    int lhs = Eval(expr->GetLeft());
    int rhs = Eval(expr->GetRight());

    switch (expr->GetOp())
    {
      case BinOp::ADD:
        return lhs + rhs;
      case BinOp::SUB:
        return lhs - rhs;
      case BinOp::DIV:
        return lhs / rhs;
      case BinOp::MUL:
        return lhs * rhs;
      case BinOp::LT:
        return lhs < rhs;
      case BinOp::LTE:
        return lhs <= rhs;
      case BinOp::GT:
        return lhs > rhs;
      case BinOp::GTE:
        return lhs >= rhs;
      case BinOp::EQ:
        return lhs == rhs;
      case BinOp::NE:
        return lhs != rhs;
      case BinOp::LOGICAL_OR:
        return lhs || rhs;
      case BinOp::LOGICAL_AND:
        return lhs && rhs;
      default:
        return 0;
    }
  }

  int EvalVar(VarExpr *expr) { return m_scope.Get(expr->GetIdentifier()); }
  int EvalNumber(NumberExpr *expr) { return expr->GetNumber(); }

  int EvalCons(ConsExpr *expr)
  {
    int last = Eval(expr->GetExpr());
    if (expr->GetNext()) return Eval(expr->GetNext());
    return last;
  }

private:
  LoopMetadata *m_loops;
  ScopeStack m_scope;
  bool m_break;
  LoopStack m_loopStack;
};


//  -=============================-
//
//  Infinite loop classification
//
//  -=============================-
class InfiniteLoopClassifierPass : public RecursiveASTVisitor
{
public:
  InfiniteLoopClassifierPass(ASTContext *context, LoopMetadata *loops) :
      RecursiveASTVisitor(context), m_loops(loops)
  {
  }

  void BeforeTraverse() override
  {
    m_insideInfiniteLoop = false;
  }

  bool VisitForStmt(ForStmt *stmt) override
  {
    Loop &loop = m_loops->At(stmt);

    if (m_insideInfiniteLoop)
    {
      loop.m_iterations = INSIDE_INFINITE_LOOP;
      stmt->GetBody()->Accept(this);
      return false;
    }

    if (loop.m_iterations < 0) m_insideInfiniteLoop = true;
    stmt->GetBody()->Accept(this);
    if (loop.m_iterations < 0) m_insideInfiniteLoop = false;
    return false;
  }

  bool VisitWhileStmt(WhileStmt *stmt) override
  {
    Loop &loop = m_loops->At(stmt);

    if (m_insideInfiniteLoop)
    {
      loop.m_iterations = INSIDE_INFINITE_LOOP;
      stmt->GetBody()->Accept(this);
      return false;
    }

    if (loop.m_iterations < 0) m_insideInfiniteLoop = true;
    stmt->GetBody()->Accept(this);
    if (loop.m_iterations < 0) m_insideInfiniteLoop = false;
    return false;
  }

  bool VisitDoStmt(DoStmt *stmt) override
  {
    Loop &loop = m_loops->At(stmt);

    if (m_insideInfiniteLoop)
    {
      loop.m_iterations = INSIDE_INFINITE_LOOP;
      stmt->GetBody()->Accept(this);
      return false;
    }

    if (loop.m_iterations < 0) m_insideInfiniteLoop = true;
    stmt->GetBody()->Accept(this);
    if (loop.m_iterations < 0) m_insideInfiniteLoop = false;
    return false;
  }

private:
  LoopMetadata *m_loops;
  bool m_insideInfiniteLoop;
};


//  -=============================-
//
//  AST consumer
//
//  -=============================-

class DumpASTConsumer : public ASTConsumer
{
public:
  void RunASTConsumer(ASTContext &ctx)
  {
    LoopMetadata loops;
    InitializePass initPass(&ctx, &loops);
    LevelPass levelPass(&ctx, &loops);
    ExitPointsPass exitPointsPass(&ctx, &loops);
    IterationsPass iterationsPass(&ctx, &loops);
    InfiniteLoopClassifierPass infiniteLoopClassifierPass(&ctx, &loops);
    initPass.Traverse();
    levelPass.Traverse();
    exitPointsPass.Traverse();
    iterationsPass.Traverse();
    infiniteLoopClassifierPass.Traverse();

    std::vector<Loop> loopVec = loops.ToVector();
    std::sort(loopVec.begin(), loopVec.end(),
              [](const Loop &a, const Loop &b) -> bool {
                return a.m_location.m_line < b.m_location.m_line;
              });

    bool printPretty = GetOptions().HasOption("pretty");
    if (printPretty)
      prettyPrinter(loopVec, ctx.GetDiag());
    else
      uglyPrinter(loopVec);
  }

private:
  void uglyPrinter(std::vector<Loop> &loops)
  {
    cout << ">>>>>>>>>>>>>>>" << endl;
    for (Loop loop : loops)
      cout << loop.ToString() << endl;
    cout << ">>>>>>>>>>>>>>>" << endl;
  }

  void prettyPrinter(std::vector<Loop> &loops, DiagnosticsEngine &diag)
  {
    for (Loop loop : loops)
    {
      std::stringstream sout;
      sout << "level: " << loop.m_level + 1 << ", ";
      sout << "exit points: " << loop.m_exitPoints << ", ";
      sout << "iterations: ";
      if (loop.m_iterations == POSSIBLE_INFINITE_LOOP) sout << "possible infinite loop";
      else if (loop.m_iterations == INSIDE_INFINITE_LOOP) sout << "inside infinite loop";
      else sout << loop.m_iterations;
      diag.Note(loop.m_location, sout.str());
    }
  }
};


int main(int argc, char **argv)
{
  Tool tool(argc, argv);
  std::unique_ptr<DumpASTConsumer> consumer(new DumpASTConsumer());
  return tool.run(std::move(consumer));
}