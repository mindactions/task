#include "AST/Decl.h"
#include "AST/Expr.h"
#include "Common/Utils.h"
#include <sstream>


// VarDecl
//
//
VarDecl::VarDecl() :
    Decl(internal::VarDeclId)
{
}


VarDecl::VarDecl(VarExpr *lhs, Expr *rhs, SourceLocation loc) :
    Decl(loc, internal::VarDeclId), m_lhs(lhs), m_rhs(rhs)
{
}


VarDecl::~VarDecl()
{
}


VarExpr *
VarDecl::GetLHS()
{
  return m_lhs;
}

void
VarDecl::SetLHS(VarExpr *lhs)
{
  m_lhs = lhs;
}


Expr *
VarDecl::GetRHS()
{
  return m_rhs;
}


void
VarDecl::SetRHS(Expr *rhs)
{
  m_rhs = rhs;
}


std::string
VarDecl::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(VarDecl" << std::endl;
  sout << m_lhs->ToString(indent + 4);
  if (m_rhs)
    sout << std::endl << m_rhs->ToString(indent + 4);
  sout << ")";
  return sout.str();
}


void
VarDecl::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitDecl(this))
  {
    m_lhs->Accept(visitor);
    if (m_rhs) m_rhs->Accept(visitor);
  }
}


// CompoundDecl
//
//
CompoundDecl::CompoundDecl() :
    Decl(internal::CompoundDeclId)
{
}


CompoundDecl::~CompoundDecl()
{
}


void
CompoundDecl::Add(Decl *decl)
{
  m_decls.push_back(decl);
}


std::list<Decl *>::iterator
CompoundDecl::Begin()
{
  return m_decls.begin();
}


std::list<Decl *>::iterator
CompoundDecl::End()
{
  return m_decls.end();
}


std::string
CompoundDecl::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(CompoundDecl";
  if (m_decls.size())
  {
    sout << std::endl;
    std::list<Decl *>::iterator beforeLast = m_decls.end();
    --beforeLast;
    for (std::list<Decl *>::iterator i = m_decls.begin(); i != beforeLast; i++)
      sout << (*i)->ToString(indent + 4) << std::endl;
    sout << (*beforeLast)->ToString(indent + 4) << ")";
  }
  else
  {
    sout << ")";
  }
  return sout.str();
}


void
CompoundDecl::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitDecl(this))
  {
    for (Decl *decl : m_decls)
      decl->Accept(visitor);
  }
}