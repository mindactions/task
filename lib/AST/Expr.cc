#include "AST/Expr.h"
#include "Common/Utils.h"
#include <sstream>


// ConsExpr
//
//
ConsExpr::ConsExpr() :
    m_expr(nullptr), m_next(nullptr), Expr(internal::ConsExprId)
{
}


ConsExpr::ConsExpr(Expr *expr, Expr *next, SourceLocation loc) :
    m_expr(expr), m_next(next), Expr(loc, internal::ConsExprId)
{
}


ConsExpr::~ConsExpr()
{
  if (m_expr) { delete m_expr; }
  if (m_next) { delete m_next; }
}


Expr *
ConsExpr::GetExpr()
{
  return m_expr;
}


void
ConsExpr::SetExpr(Expr *expr)
{
  m_expr = expr;
}


Expr *
ConsExpr::GetNext()
{
  return m_next;
}


void
ConsExpr::SetNext(Expr *expr)
{
  m_expr = expr;
}


std::string
ConsExpr::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(ConsExpr " << GetLocation() << std::endl;
  sout << m_expr->ToString(indent + 4) << std::endl;
  sout << m_next->ToString(indent + 4) << ")";
  return sout.str();
}


void
ConsExpr::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitExpr(this))
  {
    m_expr->Accept(visitor);
    m_next->Accept(visitor);
  }
}


// VarExpr node
//
//
VarExpr::VarExpr() :
    m_identifier(""), Expr(internal::VarExprId)
{
}


VarExpr::VarExpr(std::string identifier, SourceLocation loc) :
    m_identifier(identifier), Expr(loc, internal::VarExprId)
{
}


std::string
VarExpr::GetIdentifier()
{
  return m_identifier;
}


void
VarExpr::SetIdentifier(std::string identifier)
{
  m_identifier = identifier;
}


std::string
VarExpr::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(VarExpr \'" << m_identifier << "\' " <<
          GetLocation() << ")";
  return sout.str();
}


void
VarExpr::Accept(RecursiveASTVisitor *visitor)
{
  visitor->VisitExpr(this);
}


// NumberExpr
//
//
NumberExpr::NumberExpr() :
    m_value(0), Expr(internal::NumberExprId)
{
}


NumberExpr::NumberExpr(int value, SourceLocation loc) :
    m_value(value), Expr(loc, internal::NumberExprId)
{
}


int
NumberExpr::GetNumber()
{
  return m_value;
}


void
NumberExpr::SetNumber(int value)
{
  m_value = value;
}

std::string
NumberExpr::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(NumberExpr \'" << m_value << "\' "  << 
          GetLocation() << ")";
  return sout.str();
}


void
NumberExpr::Accept(RecursiveASTVisitor *visitor)
{
  visitor->VisitExpr(this);
}


// BinOpExpr
//
//
BinOpExpr::BinOpExpr() :
    m_op(BinOp::NONE), m_left(0), m_right(0), Expr(internal::BinOpExprId)
{
}


BinOpExpr::BinOpExpr(BinOp op, Expr *left, Expr *right, SourceLocation loc) :
    m_op(op), m_left(left), m_right(right), Expr(loc, internal::BinOpExprId)
{
}


BinOpExpr::~BinOpExpr()
{
  if (m_left) { delete m_left; }
  if (m_right) { delete m_right; }
}


Expr *
BinOpExpr::GetLeft()
{
  return m_left;
}


void
BinOpExpr::SetLeft(Expr *left)
{
  m_left = left;
}


Expr *
BinOpExpr::GetRight()
{
  return m_right;
}


void
BinOpExpr::SetRight(Expr *right)
{
  m_right = right;
}


BinOp
BinOpExpr::GetOp()
{
  return m_op;
}


void
BinOpExpr::SetOp(BinOp op)
{
  m_op = op;
}


std::string
BinOpExpr::ToString(int indent)
{
  std::string op = "";
  std::stringstream sout;

  switch (m_op)
  {
    case BinOp::ADD:
      op = "+";
      break;
    case BinOp::SUB:
      op = "-";
      break;
    case BinOp::MUL:
      op = "*";
      break;
    case BinOp::DIV:
      op = "/";
      break;
    case BinOp::MOV:
      op = "=";
      break;
    case BinOp::LT:
      op = "<";
      break;
    case BinOp::LTE:
      op = "<=";
      break;
    case BinOp::GT:
      op = ">";
      break;
    case BinOp::GTE:
      op = ">=";
      break;
    case BinOp::EQ:
      op = "==";
      break;
    case BinOp::NE:
      op = "!=";
      break;
    case BinOp::LOGICAL_AND:
      op = "&&";
      break;
    case BinOp::LOGICAL_OR:
      op = "||";
      break;
    case BinOp::COMMA:
      op = ",";
      break;
    default:
      break;
  }

  printIndent(sout, indent);
  sout << "(BinOpExpr \'" << op << "\' " << GetLocation() << std::endl;
  sout << m_left->ToString(indent + 4) << std::endl;
  sout << m_right->ToString(indent + 4);
  sout << ")";
  return sout.str();
}


void
BinOpExpr::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitExpr(this))
  {
    m_right->Accept(visitor);
    m_left->Accept(visitor);
  }
}


// UnaryExpr
//
//
UnaryExpr::UnaryExpr() :
    m_op(UnaryOp::NONE), m_expr(0), Expr(internal::UnaryExprId)
{
}


UnaryExpr::UnaryExpr(UnaryOp op, Expr *expr, SourceLocation loc) :
    m_op(op), m_expr(expr), Expr(loc, internal::UnaryExprId)
{
}


UnaryExpr::~UnaryExpr()
{
  if (m_expr) { delete m_expr; }
}


Expr *
UnaryExpr::GetExpr()
{
  return m_expr;
}


void
UnaryExpr::SetExpr(Expr *expr)
{
  m_expr = expr;
}


UnaryOp
UnaryExpr::GetOp()
{
  return m_op;
}


void
UnaryExpr::SetOp(UnaryOp op)
{
  m_op = op;
}


std::string
UnaryExpr::ToString(int indent)
{
  std::string op = "";
  std::stringstream sout;

  switch (m_op)
  {
    case UnaryOp::INC:
      op = "++";
      break;
    case UnaryOp::DEC:
      op = "--";
      break;
    case UnaryOp::NOT:
      op = "!";
      break;
    default:
      break;
  }

  printIndent(sout, indent);
  sout << "(UnaryExpr \'" << op << "\' " << GetLocation() << std::endl;
  sout << m_expr->ToString(indent + 4);
  sout << ")";
  return sout.str();
}


void
UnaryExpr::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitExpr(this))
    m_expr->Accept(visitor);
}