#include "AST/RecursiveASTVisitor.h"
#include "AST/Stmt.h"
#include "AST/Expr.h"
#include "AST/Decl.h"


void
RecursiveASTVisitor::Traverse()
{
  BeforeTraverse();
  m_context->GetRoot()->Accept(this);
  AfterTraverse();
}


bool
RecursiveASTVisitor::VisitExpr(Expr *expr)
{
  bool ret = false;
  BeforeExpr(expr);
  if (isa<VarExpr>(expr)) ret = VisitVarExpr(dyn_cast<VarExpr>(expr));
  else if (isa<NumberExpr>(expr)) ret = VisitNumberExpr(dyn_cast<NumberExpr>(expr));
  else if (isa<BinOpExpr>(expr)) ret = VisitBinOpExpr(dyn_cast<BinOpExpr>(expr));
  else if (isa<UnaryExpr>(expr)) ret = VisitUnaryExpr(dyn_cast<UnaryExpr>(expr));
  else if (isa<ConsExpr>(expr)) ret = VisitConsExpr(dyn_cast<ConsExpr>(expr));
  AfterExpr(expr);
  return ret;
}


bool
RecursiveASTVisitor::VisitStmt(Stmt *stmt)
{
  bool ret = false;
  BeforeStmt(stmt);
  if (isa<IfStmt>(stmt)) ret = VisitIfStmt(dyn_cast<IfStmt>(stmt));
  else if (isa<WhileStmt>(stmt)) ret = VisitWhileStmt(dyn_cast<WhileStmt>(stmt));
  else if (isa<DoStmt>(stmt)) ret = VisitDoStmt(dyn_cast<DoStmt>(stmt));
  else if (isa<ForStmt>(stmt)) ret = VisitForStmt(dyn_cast<ForStmt>(stmt));
  else if (isa<BreakStmt>(stmt)) ret = VisitBreakStmt(dyn_cast<BreakStmt>(stmt));
  else if (isa<ContinueStmt>(stmt)) ret = VisitContinueStmt(dyn_cast<ContinueStmt>(stmt));
  else if (isa<CompoundStmt>(stmt)) ret = VisitCompoundStmt(dyn_cast<CompoundStmt>(stmt));
  else if (isa<ExpressionStmt>(stmt)) ret = VisitExpressionStmt(dyn_cast<ExpressionStmt>(stmt));
  else if (isa<DeclarationStmt>(stmt)) ret = VisitDeclarationStmt(dyn_cast<DeclarationStmt>(stmt));
  AfterStmt(stmt);
  return ret;
}


bool
RecursiveASTVisitor::VisitDecl(Decl *decl)
{
  bool ret = false;
  BeforeDecl(decl);
  if (isa<VarDecl>(decl)) ret = VisitVarDecl(dyn_cast<VarDecl>(decl));
  else if (isa<CompoundDecl>(decl)) ret = VisitCompoundDecl(dyn_cast<CompoundDecl>(decl));
  AfterDecl(decl);
  return ret;
}