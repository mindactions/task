#include "AST/RecursiveDescentParser.h"
#include <cstdlib>


bool
IsDeclaration(TokenKind kind)
{
  switch (kind)
  {
    case TokenKind::INT:
      return true;
    default:
      return false;
  }
}


RecursiveDescentParser::RecursiveDescentParser(Lexer *lexer, DiagnosticsEngine *diag) :
    m_lexer(lexer), m_diag(diag)
{
}


internal::Result<Stmt>
RecursiveDescentParser::Parse()
{
  ConsumeToken(); // eat the first token
  return ParseStatements();
}


//
// Core features
//

Token
RecursiveDescentParser::NextToken()
{
  return m_lexer->Lookup(1);
}

Token
RecursiveDescentParser::ConsumeToken()
{
  m_token = m_lexer->GetNextToken();
  return m_token;
}


Token
RecursiveDescentParser::CurrentToken()
{
  return m_token;
}


Token
RecursiveDescentParser::LookAhead(int k)
{
  return m_lexer->Lookup(k - 1);
}


int
RecursiveDescentParser::SaveTokenIndex()
{
  return m_lexer->GetCurrentTokenIndex();
}


void
RecursiveDescentParser::RestoreTokenIndex(int index)
{
  m_lexer->SetCurrentTokenIndex(index);
  m_token = m_lexer->At(index);
}


void
RecursiveDescentParser::Expect(TokenKind kind)
{
  Token token = CurrentToken();

  if (token.m_kind == kind)
  {
    ConsumeToken();
  } 
  else
  {
    std::string message = "expected ";
    message += "\'" + TokenKindToString(kind) + "\', found \'" + CurrentToken().m_payload + "\'";
    Error(message);
  }
}


void
RecursiveDescentParser::DoesNotExpect(TokenKind kind)
{
  if (CurrentToken().m_kind == kind)
  {
    std::string message = "\'" + TokenKindToString(kind) + "\' is not expected";
    Error(message);
  }
}


bool
RecursiveDescentParser::ExpectSafe(TokenKind kind)
{
  if (m_token.m_kind == kind)
  {
    ConsumeToken();
    return true;
  }

  return false;
}


void
RecursiveDescentParser::Error(std::string message)
{
  m_diag->Error(CurrentToken().m_location, message);
  exit(-1);
}


//
// Expression
//

//
// expression = 
//     assignment-expression
//     expression, assignment-expression
//
// assignment-expression = 
//     conditional-expression
//     unary-expression assignment-operator assignment-expression
//
//  assignment-operator =
//     = -= += *= /=
//
// conditional-expression =
//     logical-or-expression
//
// logical-or-expression =
//     logical-and-expression
//     logical-or-expression || logical-and-expression
//
// logical-and-expression =
//     equality-expression
//     logical-and-expression && equality-expression
//
// equality-expression =
//     relational-expression
//     relational-expression == relational-expression
//     relational-expression != relational-expression
//
// relational-expression =
//     additive-expression
//     additive-expression < additive-expression
//     additive-expression > additive-expression
//     additive-expression <= additive-expression
//     additive-expression >= additive-expression
//
// additive-expression =
//     multiplicative-expression
//     additive-expression + multiplicative-expression
//     additive-expression - multiplicative-expression
//
// multiplicative-expression =
//     unary-expression
//     muliplicative-expression * unary-expression
//     muliplicative-expression / unary-expression
//     muliplicative-expression % unary-expression
//
// unary-expression =
//     postfix-expression
//     ++ unary-expression
//     -- unary-expression
//     ! unary-expression
//
// postfix-expression =
//     primary-expression
//     postfix-expression ++
//     postfix-expression --
//
// primary-expression =
//     literal
//     name
//     ( assign-expression )
//

internal::Result<Expr>
RecursiveDescentParser::ParseExpression(bool followCommas)
{
  return ParseRHSOfBinaryExpression(
    ParseAssignmentExpression(),
    static_cast<Precendence::Level>(Precendence::Comma + !followCommas)
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseAssignmentExpression()
{
  if (CurrentToken().m_kind == TokenKind::IDENTIFIER && LookAhead(1).m_kind == TokenKind::EQUAL)
  {
    return ParseRHSOfBinaryExpression(
      ParseUnaryExpression(),
      Precendence::Assignment
    );
  }

  return ParseConditionalExpression();
}


internal::Result<Expr>
RecursiveDescentParser::ParseRHSOfBinaryExpression(internal::Result<Expr> lhs, Precendence::Level minPrecendence)
{
  if (lhs.IsInvalid()) { return lhs; }

  Precendence::Level nextPrecendence = GetBinOpPrecendence(CurrentToken());

  while (true)
  {
    if (nextPrecendence < minPrecendence) { return lhs; }
    Token opToken = CurrentToken();
    ConsumeToken(); // eat bin op

    internal::Result<Expr> rhs = ParseAssignmentExpression();
    if (rhs.IsInvalid())
    {
      lhs.Invalidate();
      return rhs;
    }

    Precendence::Level thisPrecendence = nextPrecendence;
    bool isRightAssociative = opToken.m_kind == TokenKind::EQUAL;
    nextPrecendence = GetBinOpPrecendence(CurrentToken());

    if (thisPrecendence < nextPrecendence || 
        (thisPrecendence < nextPrecendence && isRightAssociative))
    {
      rhs = ParseRHSOfBinaryExpression(
        rhs,
        static_cast<Precendence::Level>(thisPrecendence + !isRightAssociative));
      if (rhs.IsInvalid())
      {
        lhs.Invalidate();
        return rhs;
      }
    }

    // merge lhs and rhs
    if (opToken.m_kind == TokenKind::COMMA)
    {
      lhs = internal::Result<Expr>(new ConsExpr(lhs.Get(), rhs.Get(), opToken.m_location));
    }
    else
    {
      BinOp binOp = GetBinOpForToken(opToken);
      lhs = internal::Result<Expr>(new BinOpExpr(binOp, lhs.Get(), rhs.Get(), opToken.m_location));
    }
  }
}


internal::Result<Expr>
RecursiveDescentParser::ParseConditionalExpression()
{
  return ParseLogicalORExpression();
}


internal::Result<Expr>
RecursiveDescentParser::ParseLogicalORExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseLogicalANDExpression(),
    Precendence::LogicalOr
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseLogicalANDExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseEqualityExpression(),
    Precendence::LogicalAnd
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseEqualityExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseRelationalExpression(),
    Precendence::Equality
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseRelationalExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseAdditiveExpression(),
    Precendence::Relational
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseAdditiveExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseMultiplicativeExpression(),
    Precendence::Additive
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseMultiplicativeExpression()
{
  return ParseRHSOfBinaryExpression(
    ParseUnaryExpression(),
    Precendence::Multiplicative
  );
}


internal::Result<Expr>
RecursiveDescentParser::ParseUnaryExpression()
{
  Token tok = CurrentToken();

  switch (tok.m_kind)
  {
    case TokenKind::IDENTIFIER:
    case TokenKind::NUMBER:
    case TokenKind::LEFT_PARENTHESIS:
      return ParsePostfixExpression();
    case TokenKind::INC:
      {
        ConsumeToken(); // eat ++
        internal::Result<Expr> base = ParseUnaryExpression();
        if (base.IsValid())
          return internal::Result<Expr>(new UnaryExpr(UnaryOp::INC, base.Get(), tok.m_location)); 
        break;
      }
    case TokenKind::DEC:
      {
        ConsumeToken(); // eat --
        internal::Result<Expr> base = ParseUnaryExpression();
        if (base.IsValid())
          return internal::Result<Expr>(new UnaryExpr(UnaryOp::DEC, base.Get(), tok.m_location));
        break;
      }
    case TokenKind::NOT:
      {
        ConsumeToken(); // eat !
        internal::Result<Expr> base = ParseUnaryExpression();
        if (base.IsValid())
          return internal::Result<Expr>(new UnaryExpr(UnaryOp::NOT, base.Get(), tok.m_location));
        break;
      }
    default:
      Error("expected identifier, number, \'(\', \'++\' or \'--\', found \'" +
            CurrentToken().m_payload + "\'");
  }

  return internal::Result<Expr>();
}


internal::Result<Expr>
RecursiveDescentParser::ParsePostfixExpression()
{
  internal::Result<Expr> expr = ParsePrimary();
  if (expr.IsInvalid()) { return expr; }

  Token tok = CurrentToken();
  if (ExpectSafe(TokenKind::INC))
  {
    return internal::Result<Expr>(new UnaryExpr(UnaryOp::INC, expr.Get(), tok.m_location));
  }
  else if (ExpectSafe(TokenKind::DEC))
  {
    return internal::Result<Expr>(new UnaryExpr(UnaryOp::DEC, expr.Get(), tok.m_location));
  }

  return expr;
}


internal::Result<Expr>
RecursiveDescentParser::ParsePrimary()
{
  Token tok = CurrentToken();

  switch (tok.m_kind)
  {
    case TokenKind::IDENTIFIER:
      {
        Expr *expr = new VarExpr(tok.m_payload, tok.m_location);
        ConsumeToken();
        return internal::Result<Expr>(expr);
      }
    case TokenKind::NUMBER:
      {
        Expr *expr = new NumberExpr(std::stoi(tok.m_payload), tok.m_location);
        ConsumeToken();
        return internal::Result<Expr>(expr);
      }
    case TokenKind::LEFT_PARENTHESIS:
      {
        ConsumeToken(); // eat (
        internal::Result<Expr> expr = ParseAssignmentExpression();
        Expect(TokenKind::RIGHT_PARENTHESIS);
        return expr;
      }
    default:
      Error("expected identifer, number or \'(\', found \'" + 
            tok.m_payload + "\'");
  }
}


//
// Parse statements
//

//
// statements ::=
//     statement
//     statements statement
//
// statement ::=
//     expression-statement
//     declaration-statement
//     selection-statement
//     iteration-statement
//     jump-statement
//     compound-statement
//
// expression-statement ::=
//     expression ;
//
// declaration-statement ::=
//     declaration
//
// selection-statement ::=
//     if ( expression ) statement
//     if ( expression ) statement else statement
//
// iteration-statement ::=
//     while ( expression ) statement
//     do statement while ( expression );
//     for (for-init-statement expression ; expression) statement
//
// for-init-statement ::=
//     expression-statement
//     declaration-statement
//
// jump-statement ::=
//     break ;
//     continue ;
//
// compound-statement ::=
//     { statement-list }
//
// statement-list ::=
//     statement
//     statement-list statement
//


internal::Result<Stmt>
RecursiveDescentParser::ParseStatements()
{
  CompoundStmt *stmts = new CompoundStmt(CurrentToken().m_location);

  while (CurrentToken().m_kind != TokenKind::END)
  {
    internal::Result<Stmt> stmt = ParseStatement();
    if (stmt.IsInvalid())
    {
      delete stmts;
      return stmt;
    }

    stmts->Add(stmt.Get());
  }

  return internal::Result<Stmt>(stmts);
}


internal::Result<Stmt>
RecursiveDescentParser::ParseStatement()
{
  // lookahead
  switch (CurrentToken().m_kind)
  {
    case TokenKind::FOR:
    case TokenKind::DO:
    case TokenKind::WHILE:
      return ParseIterationStatement();

    case TokenKind::BREAK:
    case TokenKind::CONTINUE:
      return ParseJumpStatement();

    case TokenKind::LEFT_BRACE:
      return ParseCompoundStatement();

    case TokenKind::IDENTIFIER:
    case TokenKind::NUMBER:
    case TokenKind::INC:
    case TokenKind::DEC:
    case TokenKind::LEFT_PARENTHESIS:
    case TokenKind::NOT:
    case TokenKind::SEMICOLON:
      return ParseExpressionStatement();

    case TokenKind::INT:
      return ParseDeclarationStatement();

    case TokenKind::IF:
      return ParseSelectionStatement();

    default:
      Error("unexpected token \'" + TokenKindToString(CurrentToken().m_kind) + "\'");
  }
}


internal::Result<Stmt>
RecursiveDescentParser::ParseExpressionStatement()
{
  SourceLocation loc = CurrentToken().m_location;
  // SPECIAL CASE: ';' at the beginning ?
  if (ExpectSafe(TokenKind::SEMICOLON))
    return internal::Result<Stmt>(new ExpressionStmt());
  internal::Result<Expr> expr = ParseExpression();
  if (expr.IsInvalid()) { return internal::Result<Stmt>(); }
  Expect(TokenKind::SEMICOLON);
  return internal::Result<Stmt>(new ExpressionStmt(expr.Get(), loc));
}


internal::Result<Stmt>
RecursiveDescentParser::ParseDeclarationStatement()
{
  SourceLocation loc = CurrentToken().m_location;
  internal::Result<Decl> d = ParseDeclaration();
  if (d.IsInvalid()) return internal::Result<Stmt>();
  return internal::Result<Stmt>(new DeclarationStmt(d.Get(), loc));
}


internal::Result<Stmt>
RecursiveDescentParser::ParseSelectionStatement()
{
  SourceLocation loc = CurrentToken().m_location;

  switch (CurrentToken().m_kind)
  {
    case TokenKind::IF:
      {
        internal::Result<Stmt> otherwise;

        ConsumeToken(); // eat if

        // parse condition
        Expect(TokenKind::LEFT_PARENTHESIS); // eat (
        internal::Result<Expr> cond = ParseExpression();
        if (cond.IsInvalid())
        {
          return internal::Result<Stmt>();
        }
        Expect(TokenKind::RIGHT_PARENTHESIS); // eat )

        // parse then
        internal::Result<Stmt> then = ParseStatement();
        if (then.IsInvalid())
        {
          cond.Invalidate();
          return then;
        }

        // parse else
        if (ExpectSafe(TokenKind::ELSE))
        {
          otherwise = ParseStatement();
          if (otherwise.IsInvalid())
          {
            cond.Invalidate();
            then.Invalidate();
            return internal::Result<Stmt>();
          }
        }

        return internal::Result<Stmt>(new IfStmt(cond.Get(), then.Get(), otherwise.Get(), loc));
      }
    default:
      return internal::Result<Stmt>();
  }
}


internal::Result<Stmt>
RecursiveDescentParser::ParseIterationStatement()
{
  SourceLocation loc = CurrentToken().m_location;

  switch (CurrentToken().m_kind)
  {
    case TokenKind::WHILE:
      {
        ConsumeToken(); // eat while
        Expect(TokenKind::LEFT_PARENTHESIS); // eat (
        internal::Result<Expr> cond = ParseExpression();
        if (cond.IsInvalid()) { return internal::Result<Stmt>(); }
        Expect(TokenKind::RIGHT_PARENTHESIS); // eat )
        internal::Result<Stmt> body = ParseStatement();
        if (body.IsInvalid())
        {
          cond.Invalidate();
          return body;
        }
        return internal::Result<Stmt>(new WhileStmt(cond.Get(), body.Get(), loc));
      }
    case TokenKind::DO:
      {
        ConsumeToken(); // eat do
        internal::Result<Stmt> body = ParseStatement();
        if (body.IsInvalid()) { return body; }
        Expect(TokenKind::WHILE); // eat while
        Expect(TokenKind::LEFT_PARENTHESIS); // eat (
        internal::Result<Expr> cond = ParseExpression();
        if (cond.IsInvalid())
        {
          body.Invalidate();
          return internal::Result<Stmt>();
        }
        Expect(TokenKind::RIGHT_PARENTHESIS); // eat )
        Expect(TokenKind::SEMICOLON);

        return internal::Result<Stmt>(new DoStmt(cond.Get(), body.Get(), loc));
      }
    case TokenKind::FOR:
      {
        ConsumeToken(); // eat for
        Expect(TokenKind::LEFT_PARENTHESIS); // eat (
        internal::Result<Stmt> init = ParseForInitStatement();
        if (init.IsInvalid()) { return init; }
        internal::Result<Expr> cond = ParseExpression();
        if (cond.IsInvalid())
        {
          init.Invalidate();
          return internal::Result<Stmt>();
        }
        Expect(TokenKind::SEMICOLON);
        internal::Result<Expr> step = ParseExpression();
        if (step.IsInvalid())
        {
          init.Invalidate();
          cond.Invalidate();
          return internal::Result<Stmt>();
        }
        Expect(TokenKind::RIGHT_PARENTHESIS); // eat )
        internal::Result<Stmt> body = ParseStatement();
        if (body.IsInvalid())
        {
          init.Invalidate();
          cond.Invalidate();
          step.Invalidate();
          return internal::Result<Stmt>();
        }
        return internal::Result<Stmt>(new ForStmt(init.Get(), cond.Get(), step.Get(), body.Get(), loc));
      }
    default:
      return internal::Result<Stmt>();
  }
}


internal::Result<Stmt>
RecursiveDescentParser::ParseForInitStatement()
{
  if (IsDeclaration(CurrentToken().m_kind))
    return ParseDeclarationStatement();
  return ParseExpressionStatement();
}


internal::Result<Stmt>
RecursiveDescentParser::ParseJumpStatement()
{
  SourceLocation loc = CurrentToken().m_location;

  switch (CurrentToken().m_kind)
  {
    case TokenKind::BREAK:
      {
        ConsumeToken(); // eat break
        Expect(TokenKind::SEMICOLON); // eat ;
        return internal::Result<Stmt>(new BreakStmt(loc));
      }
    case TokenKind::CONTINUE:
      {
        ConsumeToken(); // eat continue
        Expect(TokenKind::SEMICOLON); // eat ;
        return internal::Result<Stmt>(new ContinueStmt(loc));
      }
    default:
      Error("unexpected token");
  }
}


internal::Result<Stmt>
RecursiveDescentParser::ParseCompoundStatement()
{
  Expect(TokenKind::LEFT_BRACE); // eat {
  internal::Result<Stmt> stmts = ParseStatementList();
  if (stmts.IsInvalid()) { return stmts; }
  Expect(TokenKind::RIGHT_BRACE); // eat }
  return stmts;
}


internal::Result<Stmt>
RecursiveDescentParser::ParseStatementList()
{
  SourceLocation loc = CurrentToken().m_location;
  CompoundStmt *stmts = new CompoundStmt(loc);

  while (CurrentToken().m_kind != TokenKind::RIGHT_BRACE)
  {
    DoesNotExpect(TokenKind::END);

    internal::Result<Stmt> stmt = ParseStatement();
    if (stmt.IsInvalid())
    {
      delete stmts;
      return stmt;
    }

    stmts->Add(stmt.Get());
  }

  return internal::Result<Stmt>(stmts);
}


//
// Parse declarations
//

//
// declaration ::=
//     decl-simple-type declarator-list ;
//
// declarator-list ::=
//     init-declarator
//     declarator-list , init-declarator
//
// init-declarator ::=
//     declarator initializer
//
// declarator ::=
//     name
//
// initializer ::=
//     = expression
//
internal::Result<Decl>
RecursiveDescentParser::ParseDeclaration()
{
  Expect(TokenKind::INT); // eat int
  internal::Result<Decl> decl = ParseDeclaratorListDeclaration();
  if (decl.IsInvalid()) return decl;
  Expect(TokenKind::SEMICOLON); // eat ;
  return decl;
}


internal::Result<Decl>
RecursiveDescentParser::ParseDeclaratorListDeclaration()
{
  internal::Result<Decl> decl = ParseDeclaratorDeclaration();
  if (CurrentToken().m_kind != TokenKind::COMMA) return decl;

  // TODO: adapter
  // TODO: refactoring
  internal::Result<CompoundDecl> decls(new CompoundDecl());
  decls.Get()->Add(decl.Get());

  while (ExpectSafe(TokenKind::COMMA))
  {
    decl = ParseDeclaratorDeclaration();
    if (decl.IsInvalid())
    {
      decls.Invalidate();
      return internal::Result<Decl>();
    }
    decls.Get()->Add(decl.Get());
  }

  return internal::Result<Decl>(decls.Get());
}


internal::Result<Decl>
RecursiveDescentParser::ParseDeclaratorDeclaration()
{
  Expr *rhs = nullptr;
  Token identifier = CurrentToken();
  Expect(TokenKind::IDENTIFIER); // eat identifier

  if (CurrentToken().m_kind == TokenKind::EQUAL)
  {
    internal::Result<Expr> initializer = ParseInitializeDeclaration();
    if (initializer.IsInvalid()) return internal::Result<Decl>();
    rhs = initializer.Get();
  }

  return internal::Result<Decl>(new VarDecl(
     new VarExpr(identifier.m_payload, identifier.m_location),
     rhs,
     identifier.m_location
  ));
}


internal::Result<Expr>
RecursiveDescentParser::ParseInitializeDeclaration()
{
  Expect(TokenKind::EQUAL); // eat =
  return ParseExpression(false); // do not follow commas
}