#include "AST/SemanticAnalyzer.h"
#include "AST/Expr.h"
#include "AST/Stmt.h"
#include "AST/Decl.h"
#include <cstdlib>


SemanticAnalyzer::SemanticAnalyzer(ASTContext *context) :
    m_diag(context->GetDiag()), RecursiveASTVisitor(context)
{
}


SemanticAnalyzer::~SemanticAnalyzer()
{
}


bool
SemanticAnalyzer::Analyze()
{
  m_cycleCount = 0;
  m_scope.Enter();
  Traverse();
  m_scope.Leave();
  return true;
}



bool
SemanticAnalyzer::VisitIfStmt(IfStmt *stmt)
{
  m_scope.Enter();
  stmt->GetCond()->Accept(this);
  stmt->GetThen()->Accept(this);
  if (stmt->GetElse()) stmt->GetElse()->Accept(this);
  m_scope.Leave();
  return false;
}


bool
SemanticAnalyzer::VisitWhileStmt(WhileStmt *stmt)
{
  m_scope.Enter();
  m_cycleCount++;
  stmt->GetCond()->Accept(this);
  stmt->GetBody()->Accept(this);
  m_cycleCount--;
  m_scope.Leave();
  return false;
}


bool
SemanticAnalyzer::VisitDoStmt(DoStmt *stmt)
{
  m_scope.Enter();
  m_cycleCount++;
  stmt->GetBody()->Accept(this);
  stmt->GetCond()->Accept(this);
  m_cycleCount--;
  m_scope.Leave();
  return false;
}


bool
SemanticAnalyzer::VisitForStmt(ForStmt *stmt)
{
  m_scope.Enter();
  m_cycleCount++;
  stmt->GetInit()->Accept(this);
  stmt->GetCond()->Accept(this);
  stmt->GetStep()->Accept(this);
  stmt->GetBody()->Accept(this);
  m_cycleCount--;
  m_scope.Leave();
  return false;
}


bool
SemanticAnalyzer::VisitVarExpr(VarExpr *expr)
{
  if (! m_scope.Has(expr->GetIdentifier()))
    Error(expr->GetLocation(), "use of undeclared identifier \'" + expr->GetIdentifier() + "\'");
  return true;
}


bool
SemanticAnalyzer::VisitNumberExpr(NumberExpr *expr)
{
  return true;
}


bool
SemanticAnalyzer::VisitBinOpExpr(BinOpExpr *expr)
{
  expr->GetRight()->Accept(this);
  expr->GetLeft()->Accept(this);
  return false;
}


bool
SemanticAnalyzer::VisitUnaryExpr(UnaryExpr *expr)
{
  expr->GetExpr()->Accept(this);
  return false;
}


bool
SemanticAnalyzer::VisitConsExpr(ConsExpr *expr)
{
  expr->GetExpr()->Accept(this);
  expr->GetNext()->Accept(this);
  return false;
}


bool
SemanticAnalyzer::VisitVarDecl(VarDecl *decl)
{
  std::string identifer = decl->GetLHS()->GetIdentifier();
  Scope &current = m_scope.Current();

  if (current.Has(identifer))
    Error(decl->GetLHS()->GetLocation(), "redefining variable \'" + identifer + "\'");

  if (decl->GetRHS()) decl->GetRHS()->Accept(this);

  current.Set(identifer, 0);
  return false;
}


bool
SemanticAnalyzer::VisitBreakStmt(BreakStmt *b)
{
  if (!m_cycleCount)
    Error(b->GetLocation(), "\'break\' statement not in loop statement");
  return true;
}


bool
SemanticAnalyzer::VisitContinueStmt(ContinueStmt *c)
{
  if (!m_cycleCount)
    Error(c->GetLocation(), "\'continue\' statement not in loop statement");
  return true;
}


void
SemanticAnalyzer::Error(SourceLocation loc, std::string message)
{
  m_diag.Error(loc, message);
  exit(-1);
}