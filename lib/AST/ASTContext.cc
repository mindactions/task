#include "AST/ASTContext.h"
#include "AST/RecursiveDescentParser.h"
#include "AST/Stmt.h"


ASTContext::ASTContext(RecursiveDescentParser *parser, DiagnosticsEngine *diag) :
    m_diag(diag)
{
  internal::Result<Stmt> result = parser->Parse();
  m_root = result.Get();
}


ASTContext::~ASTContext()
{
  if (m_root) { delete m_root; }
}


DiagnosticsEngine &
ASTContext::GetDiag()
{
  return *m_diag;
}


std::string
ASTContext::GetASTString()
{
  if (m_root)
    return m_root->ToString();

  return "";
}


Stmt *
ASTContext::GetRoot()
{
  return m_root;
}