#include "AST/Scope.h"


Scope::Scope()
{
}


Scope::~Scope()
{
}


bool
Scope::Has(std::string name)
{
  std::map<std::string, int>::iterator i = m_variables.find(name);
  return i != m_variables.end();
}


int
Scope::Get(std::string name, int defaultValue)
{
  if (!Has(name)) { return defaultValue; }
  return m_variables[name];
}


void
Scope::Set(std::string name, int value)
{
  m_variables[name] = value;
}


int &
Scope::operator[](std::string name)
{
  return m_variables[name];
}


ScopeStack::ScopeStack()
{
}


ScopeStack::~ScopeStack()
{
}


bool
ScopeStack::Empty()
{
  return m_scopes.size() == 0;
}


void
ScopeStack::Enter()
{
  m_scopes.push_front(Scope());
}


void
ScopeStack::Leave()
{
  m_scopes.pop_front();
}


Scope &
ScopeStack::Current()
{
  return m_scopes.front();
}


bool
ScopeStack::Has(std::string name)
{
  for (std::list<Scope>::iterator i = m_scopes.begin(); i != m_scopes.end(); i++)
    if ((*i).Has(name)) return true;

  return false;
}


int
ScopeStack::Get(std::string name, int defaultValue)
{
  for (std::list<Scope>::iterator i = m_scopes.begin(); i != m_scopes.end(); i++)
  {
    if ((*i).Has(name))
      return (*i).Get(name);
  }

  return defaultValue;
}


void
ScopeStack::Set(std::string name, int value)
{
  for (std::list<Scope>::iterator i = m_scopes.begin(); i != m_scopes.end(); i++)
  {
    if ((*i).Has(name))
    {
      (*i).Set(name, value);
      break;
    }
  }
}


void
ScopeStack::Declare(std::string name, int value)
{
  Current().Set(name, value);
}