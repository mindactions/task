set(INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)
set(SOURCE_FILES Expr.cc
                 Stmt.cc
                 Decl.cc
                 RecursiveDescentParser.cc
                 Operation.cc
                 ASTContext.cc
                 RecursiveASTVisitor.cc
                 Scope.cc
                 SemanticAnalyzer.cc)
set(HEADER_FILES ${INCLUDE_DIR}/AST/Expr.h
                 ${INCLUDE_DIR}/AST/Stmt.h
                 ${INCLUDE_DIR}/AST/Decl.h
                 ${INCLUDE_DIR}/AST/ASTContext.h
                 ${INCLUDE_DIR}/AST/Operation.h
                 ${INCLUDE_DIR}/AST/Scope.h
                 ${INCLUDE_DIR}/AST/RecursiveASTVisitor.h
                 ${INCLUDE_DIR}/AST/RecursiveDescentParser.h
                 ${INCLUDE_DIR}/AST/SemanticAnalyzer.h)


add_library(AST SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(AST Lex)
generate_export_header(AST
  BASE_NAME AST
  EXPORT_MACRO_NAME AST_EXPORT
  EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/exports/AST_EXPORTS.h)