#include "AST/Operation.h"


Precendence::Level
GetBinOpPrecendence(Token token)
{
  switch (token.m_kind)
  {
    case TokenKind::COMMA:
      return Precendence::Comma;

    case TokenKind::EQUAL:
      return Precendence::Assignment;

    case TokenKind::LOGICAL_OR:
      return Precendence::LogicalOr;

    case TokenKind::LOGICAL_AND:
      return Precendence::LogicalAnd;

    case TokenKind::EQ:
    case TokenKind::NE:
      return Precendence::Equality;

    case TokenKind::LT:
    case TokenKind::GT:
    case TokenKind::LTE:
    case TokenKind::GTE:
      return Precendence::Relational;

    case TokenKind::PLUS:
    case TokenKind::MINUS:
      return Precendence::Additive;

    case TokenKind::MUL:
    case TokenKind::DIV:
      return Precendence::Multiplicative;

    case TokenKind::END:
      return Precendence::End;
    default:
      return Precendence::Unknown;
  }
}


BinOp
GetBinOpForToken(Token token)
{
  switch (token.m_kind)
  {
    case TokenKind::COMMA:
      return BinOp::COMMA;

    case TokenKind::PLUS:
      return BinOp::ADD;

    case TokenKind::MINUS:
      return BinOp::SUB;

    case TokenKind::MUL:
      return BinOp::MUL;

    case TokenKind::DIV:
      return BinOp::DIV;

    case TokenKind::LOGICAL_OR:
      return BinOp::LOGICAL_OR;

    case TokenKind::LOGICAL_AND:
      return BinOp::LOGICAL_AND;

    case TokenKind::EQ:
      return BinOp::EQ;

    case TokenKind::NE:
      return BinOp::NE;

    case TokenKind::LT:
      return BinOp::LT;

    case TokenKind::LTE:
      return BinOp::LTE;

    case TokenKind::GT:
      return BinOp::GT;

    case TokenKind::GTE:
      return BinOp::GTE;

    case TokenKind::EQUAL:
      return BinOp::MOV;
    default:
      return BinOp::NONE;
  }
}