#include "AST/Stmt.h"
#include "Common/Utils.h"
#include <sstream>


// ExpressionStmt
//
//
ExpressionStmt::ExpressionStmt() :
    m_expr(nullptr), Stmt(internal::ExpressionStmtId)
{
}


ExpressionStmt::ExpressionStmt(Expr *expr, SourceLocation loc) :
    m_expr(expr), Stmt(loc, internal::ExpressionStmtId)
{
}


ExpressionStmt::~ExpressionStmt()
{
  if (m_expr) { delete m_expr; }
}


Expr *
ExpressionStmt::GetExpr()
{
  return m_expr;
}


void
ExpressionStmt::SetExpr(Expr *expr)
{
  m_expr = expr;
}


std::string
ExpressionStmt::ToString(int indent)
{
  std::stringstream sout;

  printIndent(sout, indent);
  sout << "(ExpressionStmt " << GetLocation();
  if (m_expr) sout << std::endl << m_expr->ToString(indent + 4);
  else sout << " " << "(<<<NULL>>>)";
  sout << ")";

  return sout.str();
}


void
ExpressionStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
    if (m_expr) m_expr->Accept(visitor);
}


// DeclarationStmt
//
//
DeclarationStmt::DeclarationStmt() :
    m_decl(nullptr), Stmt(internal::DeclarationStmtId)
{
}


DeclarationStmt::DeclarationStmt(Decl *decl, SourceLocation loc) :
    m_decl(decl), Stmt(loc, internal::DeclarationStmtId)
{
}


DeclarationStmt::~DeclarationStmt()
{
}


Decl *
DeclarationStmt::GetDecl()
{
  return m_decl;
}


void
DeclarationStmt::SetDecl(Decl *decl)
{
  m_decl = decl;
}


std::string
DeclarationStmt::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(DeclarationStmt" << std::endl;
  if (m_decl)
    sout << m_decl->ToString(indent + 4);
  else
    sout << "(<<<NULL>>>)";
  sout << ")";
  return sout.str();
}


void
DeclarationStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    m_decl->Accept(visitor);
  }
}


// IfStmt
//
//
IfStmt::IfStmt() :
    m_cond(nullptr), m_then(nullptr), m_else(nullptr), Stmt(internal::IfStmtId)
{
}


IfStmt::IfStmt(Expr *cond, Stmt *then, Stmt *otherwise, SourceLocation loc) :
    m_cond(cond), m_then(then), m_else(otherwise), Stmt(loc, internal::IfStmtId)
{
}


IfStmt::~IfStmt()
{
  if (m_cond) { delete m_cond; }
  if (m_then) { delete m_then; }
  if (m_else) { delete m_else; }
}


Expr *
IfStmt::GetCond()
{
  return m_cond;
}


void
IfStmt::SetCond(Expr *cond)
{
  m_cond = cond;
}


Stmt *
IfStmt::GetThen()
{
  return m_then;
}


void
IfStmt::SetThen(Stmt *then)
{
  m_then = then;
}


Stmt *
IfStmt::GetElse()
{
  return m_else;
}


void
IfStmt::SetElse(Stmt *otherwise)
{
  m_else = otherwise;
}


std::string
IfStmt::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(IfStmt " << GetLocation() << std::endl;
  sout << m_cond->ToString(indent + 4) << std::endl;
  sout << m_then->ToString(indent + 4) << std::endl;

  if (m_else)
  {
    sout << m_else->ToString(indent + 4) << ")";
  }
  else
  {
    printIndent(sout, indent + 4);
    sout << "(<<<NULL>>>)" << ")";
  }

  return sout.str();
}


void
IfStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    m_cond->Accept(visitor);
    m_then->Accept(visitor);
    if (m_else) m_else->Accept(visitor);
  }
}


// WhileStmt
//
//
WhileStmt::WhileStmt() :
    m_cond(0), m_body(0), Stmt(internal::WhileStmtId)
{
}


WhileStmt::WhileStmt(Expr *cond, Stmt *body, SourceLocation loc) :
    m_cond(cond), m_body(body), Stmt(loc, internal::WhileStmtId)
{
}


WhileStmt::~WhileStmt()
{
  if (m_cond) { delete m_cond; }
  if (m_body) { delete m_body; }
}


Expr *
WhileStmt::GetCond()
{
  return m_cond;
}


void
WhileStmt::SetCond(Expr *cond)
{
  m_cond = cond;
}


Stmt *
WhileStmt::GetBody()
{
  return m_body;
}


void
WhileStmt::SetBody(Stmt *body)
{
  m_body = body;
}


std::string
WhileStmt::ToString(int indent)
{
  std::stringstream sout;

  printIndent(sout, indent);
  sout << "(WhileStmt " << GetLocation() << std::endl;
  sout << m_cond->ToString(indent + 4) << std::endl;
  sout << m_body->ToString(indent + 4) << ")";

  return sout.str();
}


void
WhileStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    m_cond->Accept(visitor);
    m_body->Accept(visitor);
  }
}


// DoStmt
//
//
DoStmt::DoStmt() :
    m_cond(0), m_body(0), Stmt(internal::DoStmtId)
{
}


DoStmt::DoStmt(Expr *cond, Stmt *body, SourceLocation loc) :
    m_cond(cond), m_body(body), Stmt(loc, internal::DoStmtId)
{
}


DoStmt::~DoStmt()
{
  if (m_cond) { delete m_cond; }
  if (m_body) { delete m_body; }
}


Expr *
DoStmt::GetCond()
{
  return m_cond;
}


void
DoStmt::SetCond(Expr *cond)
{
  m_cond = cond;
}


Stmt *
DoStmt::GetBody()
{
  return m_body;
}


void
DoStmt::SetBody(Stmt *body)
{
  m_body = body;
}


std::string
DoStmt::ToString(int indent)
{
  std::stringstream sout;

  printIndent(sout, indent);
  sout << "(DoStmt " << GetLocation() << std::endl;
  sout << m_cond->ToString(indent + 4) << std::endl;
  sout << m_body->ToString(indent + 4) << ")";

  return sout.str();
}


void
DoStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    m_body->Accept(visitor);
    m_cond->Accept(visitor);
  }
}


// ForStmt
//
//
ForStmt::ForStmt() :
    m_init(nullptr), m_cond(nullptr), m_step(nullptr), m_body(nullptr), Stmt(internal::ForStmtId)
{
}


ForStmt::ForStmt(Stmt *init, Expr *cond, Expr *step, Stmt *body, SourceLocation loc) :
    m_init(init), m_cond(cond), m_step(step), m_body(body), Stmt(loc, internal::ForStmtId)
{
}


ForStmt::~ForStmt()
{
  if (m_init) { delete m_init; }
  if (m_cond) { delete m_cond; }
  if (m_step) { delete m_step; }
  if (m_body) { delete m_body; }
}


Stmt *
ForStmt::GetInit()
{
  return m_init;
}


void
ForStmt::SetInit(Stmt *init)
{
  m_init = init;
}


Expr *
ForStmt::GetCond()
{
  return m_cond;
}


void
ForStmt::SetCond(Expr *cond)
{
  m_cond = cond;
}


Expr *
ForStmt::GetStep()
{
  return m_step;
}


void
ForStmt::SetStep(Expr *step)
{
  m_step = step;
}


Stmt *
ForStmt::GetBody()
{
  return m_body;
}


void
ForStmt::SetBody(Stmt *body)
{
  m_body = body;
}


std::string
ForStmt::ToString(int indent)
{
  std::stringstream sout;

  printIndent(sout, indent);
  sout << "(ForStmt " << GetLocation() << std::endl;
  sout << m_init->ToString(indent + 4) << std::endl;
  sout << m_cond->ToString(indent + 4) << std::endl;
  sout << m_step->ToString(indent + 4) << std::endl;
  sout << m_body->ToString(indent + 4) << ")";

  return sout.str();
}


void
ForStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    m_init->Accept(visitor);
    m_cond->Accept(visitor);
    m_step->Accept(visitor);
    m_body->Accept(visitor);
  }
}


// CompoundStmt
//
//
CompoundStmt::CompoundStmt() :
    Stmt(internal::CompoundStmtId)
{
}


CompoundStmt::CompoundStmt(SourceLocation loc) :
    Stmt(loc, internal::CompoundStmtId)
{
}


CompoundStmt::~CompoundStmt()
{
  for (auto child : m_stmts)
  {
    delete child;
  }
}


void
CompoundStmt::Add(Stmt *stmt)
{
  m_stmts.push_back(stmt);
}


int
CompoundStmt::Count()
{
  return m_stmts.size();
}


std::list<Stmt *>::iterator
CompoundStmt::Begin()
{
  return m_stmts.begin();
}


std::list<Stmt *>::iterator
CompoundStmt::End()
{
  return m_stmts.end();
}


std::string
CompoundStmt::ToString(int indent)
{
  std::stringstream sout;

  printIndent(sout, indent);
  sout << "(CompoundStmt " << GetLocation();

  if (m_stmts.size())
  {
    sout << std::endl;
    std::list<Stmt *>::iterator beforeLast = m_stmts.end();
    --beforeLast;
    for (std::list<Stmt *>::iterator i = m_stmts.begin(); i != beforeLast; i++)
      sout << (*i)->ToString(indent + 4) << std::endl;
    sout << (*beforeLast)->ToString(indent + 4) << ")";
  }
  else
  {
    sout << ")";
  }

  return sout.str();
}


void
CompoundStmt::Accept(RecursiveASTVisitor *visitor)
{
  if (visitor->VisitStmt(this))
  {
    for (auto i = Begin(); i != End(); i++)
    {
      (*i)->Accept(visitor);
    }
  }
}


// BreakStmt
//
//
BreakStmt::BreakStmt() :
    Stmt(internal::BreakId)
{
}


BreakStmt::BreakStmt(SourceLocation loc) :
    Stmt(loc, internal::BreakId)
{
}


BreakStmt::~BreakStmt()
{
}


std::string
BreakStmt::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(BreakStmt " << GetLocation() <<")";
  return sout.str();
}


void
BreakStmt::Accept(RecursiveASTVisitor *visitor)
{
  visitor->VisitStmt(this);
}


// ContinueStmt
//
//
ContinueStmt::ContinueStmt() :
    Stmt(internal::ContinueId)
{
}


ContinueStmt::ContinueStmt(SourceLocation loc) :
    Stmt(loc, internal::ContinueId)
{
}


ContinueStmt::~ContinueStmt()
{
}


std::string
ContinueStmt::ToString(int indent)
{
  std::stringstream sout;
  printIndent(sout, indent);
  sout << "(ContinueStmt " << GetLocation() << ")";
  return sout.str();
}


void
ContinueStmt::Accept(RecursiveASTVisitor *visitor)
{
  visitor->VisitStmt(this);
}