#include "Tooling/ASTConsumer.h"
#include "Lex/Lexer.h"
#include "AST/RecursiveDescentParser.h"
#include "AST/SemanticAnalyzer.h"
#include <iostream>


ASTConsumer::ASTConsumer() :
    m_astContext(nullptr)
{
}


ASTConsumer::~ASTConsumer()
{
  if (m_astContext) { delete m_astContext; }
}


Options &
ASTConsumer::GetOptions()
{
  return *m_options;
}


void
ASTConsumer::SetOptions(Options *options)
{
  m_options = options;
}


void
ASTConsumer::BuildAST(SourceManager *sm, DiagnosticsEngine *diag)
{
  // start lexical analysis
  Lexer lexer(sm, diag);

  // build AST
  RecursiveDescentParser parser(&lexer, diag);

  // start semantic analysis
  m_astContext = new ASTContext(&parser, diag);
  SemanticAnalyzer sa(m_astContext);
  sa.Analyze();
}


void
ASTConsumer::Start()
{
  BeforeStart();
  RunASTConsumer(*m_astContext);
  AfterStart();
}