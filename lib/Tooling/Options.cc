#include "Tooling/Options.h"
#include "Lex/DiagnosticsEngine.h"
#include <algorithm>
#include <cstdlib>


Options::Options(int argc, char **argv)
{
  parseOptions(argc, argv);
}


Options::~Options()
{
}


std::vector<std::string>
Options::GetSourceFiles()
{
  return m_sourceFiles;
}


bool
Options::HasAnySourceFiles()
{
  return m_sourceFiles.size() > 0;
}


bool
Options::HasOption(std::string option)
{
  return std::find(m_options.begin(), m_options.end(), option) != m_options.end();
}


void
Options::parseOptions(int argc, char **argv)
{
  if (argc < 2)
  {
    DiagnosticsEngine::Error("usage: tool-name --an-option source_file.cc");
    exit(-1);
  }

  std::string optionPrefix = "--";
  for (int i = 1; i < argc; i++)
  {
    std::string option = argv[i];

    if (! option.compare(0, optionPrefix.size(), optionPrefix))
    {
      m_options.push_back(option.substr(2));
    }
    else
    {
      m_sourceFiles.push_back(option);
    }
  }
}