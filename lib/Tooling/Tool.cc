#include "Tooling/Tool.h"
#include <cstdlib>


Tool::Tool(int argc, char **argv) :
    m_diag(nullptr), m_sourceManager(nullptr)
{
  m_options = new Options(argc, argv);
  if (! m_options->HasAnySourceFiles()) return;
  std::string sourceFile = m_options->GetSourceFiles()[0];
  m_sourceManager = new SourceManager(sourceFile);
  m_diag = new DiagnosticsEngine(m_sourceManager);
}


Tool::~Tool()
{
  delete m_options;
  if (m_diag) delete m_diag;
  if (m_sourceManager) delete m_sourceManager;
}


int
Tool::run(std::unique_ptr<ASTConsumer> consumer)
{
  consumer->BuildAST(m_sourceManager, m_diag);
  consumer->SetOptions(m_options);
  consumer->Start();
  return 0;
}