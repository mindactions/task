#include "Lex/Lexer.h"
#include <ctype.h>
#include <cstdlib>


Lexer::Lexer(SourceManager *sm) :
    m_sourceManager(sm), m_diag(nullptr), m_currentToken(0)
{
  Tokenize();
}


Lexer::Lexer(SourceManager *sm, DiagnosticsEngine *diag) :
    m_sourceManager(sm), m_diag(diag), m_currentToken(0)
{
  Tokenize();
}


Lexer::~Lexer()
{
}


void
Lexer::Tokenize()
{
  m_sourceManager->Reset();
  int lineNo = 0;

  while (m_sourceManager->HasNextLine())
  {
    std::string line = m_sourceManager->GetNextLine();
    TokenizeLine(line, lineNo);
    lineNo++;
  }
}


void
Lexer::TokenizeLine(std::string &line, int lineNo)
{
  int lineLength = line.size();
  int charIndex = 0;

  while (charIndex < lineLength)
  {
    char currentChar = line[charIndex];

    // current lexeme is an identifier
    if (isalpha(currentChar))
    {
      std::string identifier = "";

      do
      {
        identifier += currentChar;
        charIndex++;
      } while (charIndex < lineLength && isalnum(currentChar = line[charIndex]));

      // handle token
      Token token;
      SourceLocation sl(lineNo, charIndex - identifier.size());
      token.m_location = sl;
      token.m_length = identifier.size();
      token.m_payload = identifier;

      if (identifier == "for")
      {
        token.m_kind = TokenKind::FOR;
      }
      else if (identifier == "while")
      {
        token.m_kind = TokenKind::WHILE;
      }
      else if (identifier == "do")
      {
        token.m_kind = TokenKind::DO;
      }
      else if (identifier == "break")
      {
        token.m_kind = TokenKind::BREAK;
      }
      else if (identifier == "continue")
      {
        token.m_kind = TokenKind::CONTINUE;
      }
      else if (identifier == "if")
      {
        token.m_kind = TokenKind::IF;
      }
      else if (identifier == "else")
      {
        token.m_kind = TokenKind::ELSE;
      }
      else if (identifier == "int")
      {
        token.m_kind = TokenKind::INT;
      }
      else
      {
        token.m_kind = TokenKind::IDENTIFIER;
      }

      m_tokens.push_back(token);
    }

    // current lexeme is a number
    else if (isdigit(currentChar))
    {
      std::string number = "";

      do
      {
        number += currentChar;
        charIndex++;
      } while (charIndex < lineLength && isdigit(currentChar = line[charIndex]));

      // NUMBER token
      Token token;
      SourceLocation sl(lineNo, charIndex - number.size());
      token.m_kind = TokenKind::NUMBER;
      token.m_location = sl;
      token.m_length = number.size();
      token.m_payload = number;
      m_tokens.push_back(token);
    }

    // TODO: refactor
    // left parenthesis
    else if (currentChar == '(')
    {
      Token token;
      token.m_kind = TokenKind::LEFT_PARENTHESIS;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = "(";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // right parenthesis
    else if (currentChar == ')')
    {
      Token token;
      token.m_kind = TokenKind::RIGHT_PARENTHESIS;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = ")";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // semicolon
    else if (currentChar == ';')
    {
      Token token;
      token.m_kind = TokenKind::SEMICOLON;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = ";";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // left brace
    else if (currentChar == '{')
    {
      Token token;
      token.m_kind = TokenKind::LEFT_BRACE;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = "{";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // right brace
    else if (currentChar == '}')
    {
      Token token;
      token.m_kind = TokenKind::RIGHT_BRACE;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = "}";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // less than
    else if (currentChar == '<')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '=')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "<")
      {
        token.m_kind = TokenKind::LT;
      }
      else if (buffer == "<=")
      {
        token.m_kind = TokenKind::LTE;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // greater than
    else if (currentChar == '>')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '=')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == ">")
      {
        token.m_kind = TokenKind::GT;
      }
      else if (buffer == ">=")
      {
        token.m_kind = TokenKind::GTE;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // comma
    else if (currentChar == ',')
    {
      Token token;
      token.m_kind = TokenKind::COMMA;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = ",";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // plus or increment
    else if (currentChar == '+')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '+')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "+")
      {
        token.m_kind = TokenKind::PLUS;
      }
      else if (buffer == "++")
      {
        token.m_kind = TokenKind::INC;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // ! or !=
    else if (currentChar == '!')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '=')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "!")
      {
        token.m_kind = TokenKind::NOT;
      }
      else if (buffer == "!=")
      {
        token.m_kind = TokenKind::NE;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // = or ==
    else if (currentChar == '=')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '=')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "=")
      {
        token.m_kind = TokenKind::EQUAL;
      }
      else if (buffer == "==")
      {
        token.m_kind = TokenKind::EQ;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // logical AND
    else if (currentChar == '&')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '&')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "&&")
      {
        token.m_kind = TokenKind::LOGICAL_AND;
      }
      else
      {
        std::string message = "unknown lexeme";
        Error(token.m_location, message);
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // logical OR
    else if (currentChar == '|')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '|')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "||")
      {
        token.m_kind = TokenKind::LOGICAL_OR;
      }
      else
      {
        std::string message = "unknown lexeme";
        Error(token.m_location, message);
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // minus or decrement
    else if (currentChar == '-')
    {
      std::string buffer(1, currentChar);
      charIndex++;

      if (charIndex < lineLength && line[charIndex] == '-')
      {
        buffer += line[charIndex];
        charIndex++;
      }

      Token token;
      token.m_location = SourceLocation(lineNo, charIndex - buffer.size());
      token.m_length = buffer.size();
      token.m_payload = buffer;

      if (buffer == "-")
      {
        token.m_kind = TokenKind::MINUS;
      }
      else if (buffer == "--")
      {
        token.m_kind = TokenKind::DEC;
      }

      m_tokens.push_back(token);
    }

    // TODO: refactor
    // mul
    else if (currentChar == '*')
    {
      Token token;
      token.m_kind = TokenKind::MUL;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = "*";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // comma
    else if (currentChar == '/')
    {
      Token token;
      token.m_kind = TokenKind::DIV;
      token.m_location = SourceLocation(lineNo, charIndex);
      token.m_length = 1;
      token.m_payload = "/";
      m_tokens.push_back(token);

      charIndex++;
    }

    // TODO: refactor
    // skip whitespace
    else if (isspace(currentChar))
    {
      while (charIndex < lineLength && isspace(currentChar = line[charIndex]))
      {
        charIndex++;
      }
    }

    else
    {
      std::string message = "unknown lexeme";
      Error(SourceLocation(lineNo, charIndex), message);
    }
  }
}


void
Lexer::Reset()
{
  m_currentToken = 0;
}


bool
Lexer::HasNextToken()
{
  return m_currentToken < m_tokens.size();
}


Token
Lexer::GetNextToken()
{
  if (! HasNextToken()) { return GetEndToken(); }
  return m_tokens[m_currentToken++];
}


Token
Lexer::Lookup(int at)
{
  if (m_currentToken + at >= m_tokens.size()) { return GetEndToken(); }
  return m_tokens[m_currentToken + at];
}


Token
Lexer::At(int index)
{
  return m_tokens[index];
}


SourceManager&
Lexer::GetSourceManager()
{
  return *m_sourceManager;
}


int
Lexer::GetCurrentTokenIndex()
{
  return m_currentToken;
}


void
Lexer::SetCurrentTokenIndex(int index)
{
  m_currentToken = index;
}


Token
Lexer::GetEndToken()
{
  int totalLines = m_sourceManager->GetTotalLines();
  if (totalLines)
  {
    int line = totalLines - 1;
    int column = m_sourceManager->GetLine(line).size();
    return Token(TokenKind::END, SourceLocation(line, column));
  }

  return Token(TokenKind::END);
}


void 
Lexer::Error(SourceLocation loc, std::string message)
{
  if (m_diag) m_diag->Error(loc, message);
  exit(-1);
}