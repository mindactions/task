#include "Lex/SourceManager.h"
#include "Lex/DiagnosticsEngine.h"
#include <fstream>
#include <cstdlib>
#include <sstream>


SourceManager::SourceManager() :
    m_currentLine(-1), m_filename("")
{
}


SourceManager::SourceManager(std::string filename) :
    m_currentLine(0), m_filename(filename)
{
  std::ifstream fin(filename);
  std::string line;

  if (! fin.is_open())
  {
    DiagnosticsEngine::Error("file not found");
    exit(-1);
  }

  while (std::getline(fin, line))
  {
    m_lines.push_back(line);
  }

  fin.close();
}

SourceManager::SourceManager(const Buffer &buffer)
{
  std::string text = buffer.getData();
  std::istringstream sin(text);
  std::string line;
  while (std::getline(sin, line, '\n')){
    m_lines.push_back(line);
  }

}

SourceManager::~SourceManager()
{
}


void
SourceManager::Reset()
{
  m_currentLine = 0;
}


std::string
SourceManager::GetNextLine()
{
  if (! HasNextLine()) { throw std::string("Buffer is empty"); }
  return m_lines[m_currentLine++];
}


bool
SourceManager::HasNextLine()
{
  return m_currentLine < GetTotalLines();
}


std::string
SourceManager::At(SourceLocation &loc, int length)
{
  int line = loc.m_line;
  if (line >= GetTotalLines()) { throw std::string("Invalid lineno"); }
  return m_lines[line].substr(loc.m_column, length);
}


std::string
SourceManager::GetFilename()
{
  return m_filename;
}


int
SourceManager::GetTotalLines()
{
  return m_lines.size();
}


std::string
SourceManager::GetLine(int lineNo)
{
  return m_lines[lineNo];
}
