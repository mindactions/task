#include "Lex/DiagnosticsEngine.h"


DiagnosticsEngine::DiagnosticsEngine(SourceManager *sm) :
    m_sm(sm)
{
}


DiagnosticsEngine::~DiagnosticsEngine()
{
}


void
DiagnosticsEngine::DiagnosticsEngine::Note(std::string message)
{
  std::cout << colors::cyan << "note: " << colors::reset << message << std::endl;
}


void
DiagnosticsEngine::DiagnosticsEngine::Note(SourceLocation loc, std::string message)
{
  // print source filename, line and column
  std::cout << m_sm->GetFilename() << ":" << loc.m_line + 1 << ":" << loc.m_column + 1 << " ";
  std::cout << colors::cyan << "note: " << colors::reset << message << std::endl;

  // print line
  std::string line = m_sm->GetLine(loc.m_line);
  std::cout << "\t" << line << std::endl;

  // print caret
  std::cout << "\t";
  for (int i = 0; i < loc.m_column; i++)
  {
    if (line[i] == '\t')
      std::cout << '\t';
    else
      std::cout << ' ';
  }
  std::cout << colors::green << '^' << colors::reset << std::endl;
}


void
DiagnosticsEngine::DiagnosticsEngine::Warning(std::string message)
{
  std::cout << colors::yellow << "warning: " << colors::reset << message << std::endl;
}


void
DiagnosticsEngine::DiagnosticsEngine::Warning(SourceLocation loc, std::string message)
{
  // print source filename, line and column
  std::cout << m_sm->GetFilename() << ":" << loc.m_line + 1 << ":" << loc.m_column + 1 << " ";
  std::cout << colors::yellow << "warning: " << colors::reset << message << std::endl;

  // print line
  std::string line = m_sm->GetLine(loc.m_line);
  std::cout << "\t" << line << std::endl;

  // print caret
  std::cout << "\t";
  for (int i = 0; i < loc.m_column; i++)
  {
    if (line[i] == '\t')
      std::cout << '\t';
    else
      std::cout << ' ';
  }
  std::cout << colors::green << '^' << colors::reset << std::endl;
}


void
DiagnosticsEngine::DiagnosticsEngine::Error(std::string message)
{
  std::cout << colors::red << "error: " << colors::reset << message << std::endl;
}


void
DiagnosticsEngine::DiagnosticsEngine::Error(SourceLocation loc, std::string message)
{
  // print source filename, line and column
  std::cout << m_sm->GetFilename() << ":" << loc.m_line + 1 << ":" << loc.m_column + 1 << " ";
  std::cout << colors::red << "error: " << colors::reset << message << std::endl;

  // print line
  std::string line = m_sm->GetLine(loc.m_line);
  std::cout << "\t" << line << std::endl;

  // print caret
  std::cout << "\t";
  for (int i = 0; i < loc.m_column; i++)
  {
    if (line[i] == '\t')
      std::cout << '\t';
    else
      std::cout << ' ';
  }
  std::cout << colors::green << '^' << colors::reset << std::endl;
}
