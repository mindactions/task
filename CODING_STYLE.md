# Coding style


## Indentation

2 spaces

```cpp
// OK
if (ok)
{
  SayHolla();
}

// WTF?
if (ok)
{
      SayHolla();
}
```


## snake_case or CamelCase?

CamelCase. First letter is uppercase:

```cpp
// OK
void DoSomething();

void doSomething();
// WTF?
```

## Return type

Place return type of a function or method at separate line:

```cpp
// OK
void
DoSomething()
{
  ...
}

void
Dude::DoSomething()
{
  ...
}

// WTF?
void DoSomething()
{
}

void Dude::DoSomething()
{
  ...
}
```


## Space between functions/classes

2 blank lines


```cpp
// OK
void
DoSomething()
{
  ...
}


void
AnotherFunction()
{
}

// WTF x1
void
DoSomething()
{
}

void
AnotherFunction()
{
}


// WTF x2
void
DoSomething()
{
}
void
AnotherFunction()
{
}
```

## Space after last `#include`

2 blank lines

```cpp
// OK
#include <iostream
#include <fancy-stuff.h>


void DefineSomeStuff()

// WTF 1
#include <iostream
#include <fancy-stuff.h>

void DefineSomeStuff()

// WTF 2
#include <iostream
#include <fancy-stuff.h>





void DefineSomeStuff()
```

## Put space between left parenthesis and a keyword

```cpp
// OK

if (ok)
{
}

for (;;)
{
  ...
}

// WTF

if(ok)
{
}

for(;;)
{
}
```

## It is not JavaScript

Put curly braces on the new line:

```cpp
// OK
int f()
{
}

// WTF? It is not JavaScript or Java
int f() {
}
```


## Function/Method prototype is too long :(

```cpp
// OK
void f(
  int a,
  int b,
  int c
);

void f(int a
       int b,
       int c);

// WTF?
void f(int a,
  int b, int c);

void f(int a,
  int b,
  int c);
```