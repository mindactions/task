#include "gtest/gtest.h"
#include "AST/Expr.h"
#include "AST/Stmt.h"
#include "Common/Object.h"


TEST(EXPR, isa_VarExpr)
{
  Expr *e = new VarExpr();
  ASSERT_EQ(dyn_cast<VarExpr>(e), e);
  ASSERT_EQ(dyn_cast<NumberExpr>(e), nullptr);
}


TEST(EXPR, isa_NumberExpr)
{
  Expr *e = new NumberExpr();
  ASSERT_EQ(dyn_cast<NumberExpr>(e), e);
  ASSERT_EQ(dyn_cast<VarExpr>(e), nullptr);
}


TEST(EXPR, isa_BinOpExpr)
{
  Expr *e = new BinOpExpr();
  ASSERT_EQ(dyn_cast<BinOpExpr>(e), e);
  ASSERT_EQ(dyn_cast<VarExpr>(e), nullptr);
}


TEST(EXPR, isa_UnaryExpr)
{
  Expr *e = new UnaryExpr();
  ASSERT_EQ(dyn_cast<UnaryExpr>(e), e);
  ASSERT_EQ(dyn_cast<VarExpr>(e), nullptr);
}


TEST(EXPR, isa_ConsExpr)
{
  Expr *e = new ConsExpr();
  ASSERT_EQ(dyn_cast<ConsExpr>(e), e);
  ASSERT_EQ(dyn_cast<VarExpr>(e), nullptr);
}


TEST(STMT, isa_ExpressionStmt)
{
  Stmt *s = new ExpressionStmt();
  ASSERT_EQ(dyn_cast<ExpressionStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_DeclarationStmt)
{
  Stmt *s = new DeclarationStmt();
  ASSERT_EQ(dyn_cast<DeclarationStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_IfStmt)
{
  Stmt *s = new IfStmt();
  ASSERT_EQ(dyn_cast<IfStmt>(s), s);
  ASSERT_EQ(dyn_cast<ExpressionStmt>(s), nullptr);
}


TEST(STMT, isa_WhileStmt)
{
  Stmt *s = new WhileStmt();
  ASSERT_EQ(dyn_cast<WhileStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_DoStmt)
{
  Stmt *s = new DoStmt();
  ASSERT_EQ(dyn_cast<DoStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_ForStmt)
{
  Stmt *s = new ForStmt();
  ASSERT_EQ(dyn_cast<ForStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_CompoundStmt)
{
  Stmt *s = new CompoundStmt();
  ASSERT_EQ(dyn_cast<CompoundStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_BreakStmt)
{
  Stmt *s = new BreakStmt();
  ASSERT_EQ(dyn_cast<BreakStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(STMT, isa_ContinueStmt)
{
  Stmt *s = new ContinueStmt();
  ASSERT_EQ(dyn_cast<ContinueStmt>(s), s);
  ASSERT_EQ(dyn_cast<IfStmt>(s), nullptr);
}


TEST(DECL, isa_VarDecl)
{
  Decl *d = new VarDecl();
  ASSERT_EQ(dyn_cast<VarDecl>(d), d);
  ASSERT_EQ(dyn_cast<CompoundDecl>(d), nullptr);
}


TEST(DECL, isa_CompoundDecl)
{
  Decl *d = new CompoundDecl();
  ASSERT_EQ(dyn_cast<CompoundDecl>(d), d);
  ASSERT_EQ(dyn_cast<VarDecl>(d), nullptr);
}