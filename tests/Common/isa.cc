#include "gtest/gtest.h"
#include "AST/Expr.h"
#include "AST/Stmt.h"
#include "Common/Object.h"


TEST(EXPR, isa_VarExpr)
{
  Expr *e = new VarExpr();
  ASSERT_TRUE(isa<VarExpr>(e));
}


TEST(EXPR, isa_NumberExpr)
{
  Expr *e = new NumberExpr();
  ASSERT_TRUE(isa<NumberExpr>(e));
}


TEST(EXPR, isa_BinOpExpr)
{
  Expr *e = new BinOpExpr();
  ASSERT_TRUE(isa<BinOpExpr>(e));
}


TEST(EXPR, isa_UnaryExpr)
{
  Expr *e = new UnaryExpr();
  ASSERT_TRUE(isa<UnaryExpr>(e));
}


TEST(EXPR, isa_ConsExpr)
{
  Expr *e = new ConsExpr();
  ASSERT_TRUE(isa<ConsExpr>(e));
}


TEST(STMT, isa_ExpressionStmt)
{
  Stmt *s = new ExpressionStmt();
  ASSERT_TRUE(isa<ExpressionStmt>(s));
}


TEST(STMT, isa_DeclarationStmt)
{
  Stmt *s = new DeclarationStmt();
  ASSERT_TRUE(isa<DeclarationStmt>(s));
}


TEST(STMT, isa_IfStmt)
{
  Stmt *s = new IfStmt();
  ASSERT_TRUE(isa<IfStmt>(s));
}


TEST(STMT, isa_WhileStmt)
{
  Stmt *s = new WhileStmt();
  ASSERT_TRUE(isa<WhileStmt>(s));
}


TEST(STMT, isa_DoStmt)
{
  Stmt *s = new DoStmt();
  ASSERT_TRUE(isa<DoStmt>(s));
}


TEST(STMT, isa_ForStmt)
{
  Stmt *s = new ForStmt();
  ASSERT_TRUE(isa<ForStmt>(s));
}


TEST(STMT, isa_CompoundStmt)
{
  Stmt *s = new CompoundStmt();
  ASSERT_TRUE(isa<CompoundStmt>(s));
}


TEST(STMT, isa_BreakStmt)
{
  Stmt *s = new BreakStmt();
  ASSERT_TRUE(isa<BreakStmt>(s));
}


TEST(STMT, isa_ContinueStmt)
{
  Stmt *s = new ContinueStmt();
  ASSERT_TRUE(isa<ContinueStmt>(s));
}


TEST(DECL, isa_VarDecl)
{
  Decl *d = new VarDecl();
  ASSERT_TRUE(isa<VarDecl>(d));
}


TEST(DECL, isa_CompoundDecl)
{
  Decl *d = new CompoundDecl();
  ASSERT_TRUE(isa<CompoundDecl>(d));
}