#include "gtest/gtest.h"
#include "AST/Scope.h"


TEST(Scope, ScopeHas)
{
  Scope scope;

  scope["var"] = 432;
  ASSERT_TRUE(scope.Has("var"));
  ASSERT_FALSE(scope.Has("blah-blah"));
}


TEST(Scope, ScopeGetSet)
{
  Scope scope;

  scope["var"] = 432;
  scope.Set("raw", 123);

  ASSERT_EQ(scope.Get("var"), 432);
  ASSERT_EQ(scope["var"], 432);
  ASSERT_EQ(scope.Get("raw"), 123);
  ASSERT_EQ(scope["raw"], 123);
}


TEST(ScopeStack, ScopeHas)
{
  ScopeStack scopes;

  ASSERT_FALSE(scopes.Has("uuuu"));

  {
    scopes.Enter();
    int j = 123;
    int b = 6;
    scopes.Set("j", j);
    scopes.Set("b", b);
    ASSERT_TRUE(scopes.Has("j"));
    ASSERT_TRUE(scopes.Has("b"));
    ASSERT_FALSE(scopes.Has("l"));

    {
      scopes.Enter();
      int l = 5;
      scopes.Set("l", l);
      ASSERT_TRUE(scopes.Has("j"));
      ASSERT_TRUE(scopes.Has("b"));
      ASSERT_TRUE(scopes.Has("l"));
      scopes.Leave();
    }

    scopes.Leave();
  }

  ASSERT_FALSE(scopes.Has("j"));
  ASSERT_FALSE(scopes.Has("b"));
  ASSERT_FALSE(scopes.Has("l"));
}


TEST(ScopeStack, ScopeGetSet)
{
  ScopeStack scopes;

  {
    scopes.Enter();
    int j = 123;
    int b = 6;
    int o = -123;
    scopes.Set("j", j);
    scopes.Set("b", b);
    scopes.Set("o", o);
    ASSERT_EQ(scopes.Get("j"), j);
    ASSERT_EQ(scopes.Get("b"), b);
    ASSERT_EQ(scopes.Get("o"), o);

    {
      scopes.Enter();
      int o2 = 5;
      scopes.Set("o", o2);
      ASSERT_EQ(scopes.Get("j"), j);
      ASSERT_EQ(scopes.Get("b"), b);
      ASSERT_EQ(scopes.Get("o"), o2);
      scopes.Leave();
    }

    ASSERT_EQ(scopes.Get("o"), o);
    scopes.Leave();
  }
}