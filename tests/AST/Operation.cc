#include "gtest/gtest.h"
#include "AST/Operation.h"
#include "Lex/Token.h"


TEST(Operation, GetBinOpPrecendence)
{
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::COMMA)), Precendence::Comma);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::EQUAL)), Precendence::Assignment);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::LOGICAL_OR)), Precendence::LogicalOr);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::LOGICAL_AND)), Precendence::LogicalAnd);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::EQ)), Precendence::Equality);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::NE)), Precendence::Equality);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::LT)), Precendence::Relational);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::LTE)), Precendence::Relational);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::GT)), Precendence::Relational);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::GTE)), Precendence::Relational);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::PLUS)), Precendence::Additive);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::MINUS)), Precendence::Additive);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::MUL)), Precendence::Multiplicative);
  ASSERT_EQ(GetBinOpPrecendence(Token(TokenKind::DIV)), Precendence::Multiplicative);
}


TEST(Operation, GetBinOpForToken)
{
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::COMMA)), BinOp::COMMA);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::PLUS)), BinOp::ADD);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::MINUS)), BinOp::SUB);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::MUL)), BinOp::MUL);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::DIV)), BinOp::DIV);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::LOGICAL_OR)), BinOp::LOGICAL_OR);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::LOGICAL_AND)), BinOp::LOGICAL_AND);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::EQ)), BinOp::EQ);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::NE)), BinOp::NE);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::LT)), BinOp::LT);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::LTE)), BinOp::LTE);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::GT)), BinOp::GT);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::GTE)), BinOp::GTE);
  ASSERT_EQ(GetBinOpForToken(Token(TokenKind::EQUAL)), BinOp::MOV);
}