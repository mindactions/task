#include "gtest/gtest.h"
#include "Lex/SourceManager.h"
#include "Lex/Lexer.h"


TEST(Lexer, Tokenize)
{
  SourceManager sm("Lex_LexerSample.cc");
  Lexer lexer(&sm);
  Token token;

  //
  // F
  // *
  // C
  // K
  //
  // YEAH
  //
  ASSERT_EQ(lexer.HasNextToken(), true);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i = 32;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::EQUAL);
  token = lexer.GetNextToken();
  ASSERT_EQ(token.m_kind, TokenKind::NUMBER);
  ASSERT_EQ(token.m_payload, "32");
  ASSERT_EQ(token.m_length, 2);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::WHILE); // while (
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_PARENTHESIS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i < 56
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LT);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_PARENTHESIS); // )
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_BRACE); // {
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // k = 5;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::EQUAL);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::DO); // do {
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_BRACE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // l++;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::INC);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // r--;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::DEC);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::BREAK); // break;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_BRACE); // } 
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::WHILE); // while (
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_PARENTHESIS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // l > 5
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::GT);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_PARENTHESIS); // );
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::FOR); // for (
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_PARENTHESIS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i = 0;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::EQUAL);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i<k + 1 - b;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LT);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::PLUS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::MINUS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i++
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::INC);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_PARENTHESIS); // )
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_BRACE); // {
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // b--
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::DEC);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::BREAK); // break;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::CONTINUE); // continue;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_BRACE); // }
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // k == 5;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::EQ);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // k != 3;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IF); // if (i && b || !c) 
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_PARENTHESIS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LOGICAL_AND);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LOGICAL_OR);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NOT);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_PARENTHESIS);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_BRACE); // {}
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_BRACE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::ELSE); // else {}
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LEFT_BRACE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_BRACE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // i * 5, b / 6;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::MUL);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::COMMA);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::DIV);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // a <= b
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::LTE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::COMMA); // ,
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER); // a >= b ;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::GTE);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::INT); // int i = 0;
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::IDENTIFIER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::EQUAL);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::NUMBER);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::SEMICOLON);
  ASSERT_EQ(lexer.GetNextToken().m_kind, TokenKind::RIGHT_BRACE); // }
  ASSERT_EQ(lexer.HasNextToken(), false);
}


TEST(Lexer, Reset)
{
  SourceManager sm("Lex_LexerSample.cc");
  Lexer lexer(&sm);

  while (lexer.HasNextToken())
  {
    lexer.GetNextToken();
  }

  ASSERT_EQ(lexer.HasNextToken(), false);
  lexer.Reset();
  ASSERT_EQ(lexer.HasNextToken(), true);
}