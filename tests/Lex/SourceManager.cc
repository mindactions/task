#include "gtest/gtest.h"
#include "Lex/SourceManager.h"


TEST(SourceManager, CreateManager)
{
  SourceManager sm("Lex_SourceManagerSample.cc");
  ASSERT_EQ(sm.GetTotalLines(), 5);
}


TEST(SourceManager, GetNextLine)
{
  SourceManager sm("Lex_SourceManagerSample.cc");

  ASSERT_EQ(sm.HasNextLine(), true);
  ASSERT_EQ(sm.GetNextLine(), "while (i < j) {");
  ASSERT_EQ(sm.GetNextLine(), "  do {");
  ASSERT_EQ(sm.GetNextLine(), "    k = 0;");
  ASSERT_EQ(sm.GetNextLine(), "  } while (x > h)");
  ASSERT_EQ(sm.GetNextLine(), "}");
  ASSERT_EQ(sm.HasNextLine(), false);
}