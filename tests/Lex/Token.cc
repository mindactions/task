#include "gtest/gtest.h"
#include "Lex/Token.h"


TEST(Token, TokenKindToString)
{
  ASSERT_EQ(TokenKindToString(TokenKind::WHILE), "while");
  ASSERT_EQ(TokenKindToString(TokenKind::DO), "do");
  ASSERT_EQ(TokenKindToString(TokenKind::FOR), "for");
  ASSERT_EQ(TokenKindToString(TokenKind::BREAK), "break");
  ASSERT_EQ(TokenKindToString(TokenKind::IF), "if");
  ASSERT_EQ(TokenKindToString(TokenKind::ELSE), "else");
  ASSERT_EQ(TokenKindToString(TokenKind::CONTINUE), "continue");
  ASSERT_EQ(TokenKindToString(TokenKind::LEFT_PARENTHESIS), "(");
  ASSERT_EQ(TokenKindToString(TokenKind::RIGHT_PARENTHESIS), ")");
  ASSERT_EQ(TokenKindToString(TokenKind::LEFT_BRACE), "{");
  ASSERT_EQ(TokenKindToString(TokenKind::RIGHT_BRACE), "}");
  ASSERT_EQ(TokenKindToString(TokenKind::SEMICOLON), ";");
  ASSERT_EQ(TokenKindToString(TokenKind::COMMA), ",");
  ASSERT_EQ(TokenKindToString(TokenKind::LT), "<");
  ASSERT_EQ(TokenKindToString(TokenKind::LTE), "<=");
  ASSERT_EQ(TokenKindToString(TokenKind::GT), ">");
  ASSERT_EQ(TokenKindToString(TokenKind::GTE), ">=");
  ASSERT_EQ(TokenKindToString(TokenKind::EQ), "==");
  ASSERT_EQ(TokenKindToString(TokenKind::NE), "!=");
  ASSERT_EQ(TokenKindToString(TokenKind::NOT), "!");
  ASSERT_EQ(TokenKindToString(TokenKind::EQUAL), "=");
  ASSERT_EQ(TokenKindToString(TokenKind::INC), "++");
  ASSERT_EQ(TokenKindToString(TokenKind::PLUS), "+");
  ASSERT_EQ(TokenKindToString(TokenKind::DEC), "--");
  ASSERT_EQ(TokenKindToString(TokenKind::MINUS), "-");
  ASSERT_EQ(TokenKindToString(TokenKind::MUL), "*");
  ASSERT_EQ(TokenKindToString(TokenKind::DIV), "/");
  ASSERT_EQ(TokenKindToString(TokenKind::LOGICAL_OR), "||");
  ASSERT_EQ(TokenKindToString(TokenKind::LOGICAL_AND), "&&");
  ASSERT_EQ(TokenKindToString(TokenKind::INT), "int");
}