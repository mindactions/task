#ifndef OBJECT_H
#define OBJECT_H


// Is a? [custom RTTI]
template<typename To, typename From>
bool 
isa(From const *f)
{
  return To::classof(f);
}


// Try to cast to given type [custom RTTI]
template<typename To, typename From>
To *
dyn_cast(From *f)
{
  if (To::classof(f))
    return reinterpret_cast<To *>(f);
  return nullptr;
}

#endif /* OBJECT_H */