#ifndef UTILS_H
#define UTILS_H

#include <ostream>
#include "Lex/SourceLocation.h"


// Print blank space
inline void
printIndent(std::ostream &sout, int indent)
{
  for (int i = 0; i < indent; i++)
    sout << " ";
}

#endif /* UTILS_H */