#ifndef DECL_H
#define DECL_H

#include "Lex/SourceLocation.h"
#include "RecursiveASTVisitor.h"
#include "AST_EXPORTS.h"
#include <string>
#include <list>


class VarDecl;
class Expr;


namespace internal
{
  enum DeclId
  {
    VarDeclId,
    CompoundDeclId
  };
}


// Base class for declarations
//
//
class AST_EXPORT Decl
{
public:
  Decl(internal::DeclId id) :
      m_id(id)
  {
  }
  Decl(SourceLocation loc, internal::DeclId id) :
      m_id(id), m_location(loc)
  {
  }
  virtual ~Decl() {}

  internal::DeclId getId() const { return m_id; }
  static bool classof(Decl const *) { return true; }

  SourceLocation GetLocation() { return m_location; }
  void SetLocation(SourceLocation loc) { m_location = loc; }

  virtual std::string ToString(int indent = 0) = 0;
  virtual void Accept(RecursiveASTVisitor *visitor) = 0;

private:
  internal::DeclId m_id;
  SourceLocation m_location;
};


// AST node for a declaration
//    for example, int i = 0;
//                 int i;
//
class AST_EXPORT VarDecl : public Decl
{
public:
  VarDecl();
  VarDecl(VarExpr *lhs, Expr *rhs, SourceLocation loc);
  ~VarDecl();

  static bool classof(VarDecl const *) { return true; }
  static bool classof(Decl const *d) { return d->getId() == internal::VarDeclId; }

  VarExpr *GetLHS();
  void SetLHS(VarExpr *lhs);
  Expr *GetRHS();
  void SetRHS(Expr *rhs);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  VarExpr *m_lhs;
  Expr *m_rhs;
};


// AST node for declaration list
//    for example, int i = 0, j = 2;
//
class AST_EXPORT CompoundDecl : public Decl
{
public:
  CompoundDecl();
  ~CompoundDecl();

  static bool classof(CompoundDecl const *) { return true; }
  static bool classof(Decl const *d) { return d->getId() == internal::CompoundDeclId; }

  void Add(Decl *decl);
  std::list<Decl *>::iterator Begin();
  std::list<Decl *>::iterator End();

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  std::list<Decl *> m_decls;
};

#endif /* DECL_H */
