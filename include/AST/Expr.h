#ifndef EXPR_H
#define EXPR_H

#include "Lex/SourceLocation.h"
#include "Operation.h"
#include "RecursiveASTVisitor.h"
#include "AST_EXPORTS.h"
#include <string>
#include <list>


namespace internal
{
  // Expr ids [it is needed for our RTTI]
  enum ExprId
  {
    VarExprId,
    NumberExprId,
    BinOpExprId,
    UnaryExprId,
    ConsExprId
  };
}


class AST_EXPORT Expr
{
public:
  Expr(internal::ExprId id) :
      m_id(id)
  {
  }
  Expr(SourceLocation loc, internal::ExprId id) :
      m_location(loc), m_id(id)
  {
  }
  virtual ~Expr() {}

  internal::ExprId getId() const { return m_id; }
  static bool classof(Expr const *) { return true; }

  SourceLocation GetLocation() { return m_location; }
  void SetLocation(SourceLocation loc) { m_location = loc; }

  virtual std::string ToString(int indent = 0) = 0;
  virtual void Accept(RecursiveASTVisitor *visitor) = 0;

private:
  internal::ExprId m_id;
  SourceLocation m_location;
};


// ConsExpr
//
// Comma separated expressions
class AST_EXPORT ConsExpr : public Expr
{
public:
  ConsExpr();
  ConsExpr(Expr *expr, Expr *next, SourceLocation loc);
  ~ConsExpr();

  static bool classof(ConsExpr const *) { return true; }
  static bool classof(Expr const *e) { return e->getId() == internal::ConsExprId; }

  Expr *GetExpr();
  void SetExpr(Expr *expr);
  Expr *GetNext();
  void SetNext(Expr *expr);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_expr;
  Expr *m_next;
};


// VarExpr
//
// Node that represents an identifier
class AST_EXPORT VarExpr : public Expr
{
public:
  VarExpr();
  VarExpr(std::string identifier, SourceLocation loc);

  static bool classof(VarExpr const *) { return true; }
  static bool classof(Expr const *e) { return e->getId() == internal::VarExprId; }

  std::string GetIdentifier();
  void SetIdentifier(std::string identifier);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  std::string m_identifier;
};


// NumberExpr
//
// Node that represents a number
class AST_EXPORT NumberExpr : public Expr
{
public:
  NumberExpr();
  NumberExpr(int value, SourceLocation loc);

  static bool classof(VarExpr const *) { return true; }
  static bool classof(Expr const *e) { return e->getId() == internal::NumberExprId; }

  int GetNumber();
  void SetNumber(int value);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  int m_value;
};


// BinOpExpr
//
// Node that reprensents a binary expression
//    like addition or subtraction
class AST_EXPORT BinOpExpr : public Expr
{
public:
  BinOpExpr();
  BinOpExpr(BinOp op,
            Expr *left,
            Expr *right,
            SourceLocation loc);
  ~BinOpExpr();

  static bool classof(VarExpr const *) { return true; }
  static bool classof(Expr const *e) { return e->getId() == internal::BinOpExprId; }

  Expr *GetLeft();
  void SetLeft(Expr *left);
  Expr *GetRight();
  void SetRight(Expr *right);
  BinOp GetOp();
  void SetOp(BinOp op);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_left;
  Expr *m_right;
  BinOp m_op;
};


// UnaryExpr
//
// Node that represents unary operation like
//    not or increment
class AST_EXPORT UnaryExpr : public Expr
{
public:
  UnaryExpr();
  UnaryExpr(UnaryOp op,
            Expr *expr,
            SourceLocation loc);
  ~UnaryExpr();

  static bool classof(VarExpr const *) { return true; }
  static bool classof(Expr const *e) { return e->getId() == internal::UnaryExprId; }

  Expr *GetExpr();
  void SetExpr(Expr *expr);
  UnaryOp GetOp();
  void SetOp(UnaryOp op);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_expr;
  UnaryOp m_op;
};

#endif /* EXPR_H */