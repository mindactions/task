#ifndef STMT_H
#define STMT_H

#include <string>
#include <list>
#include "Lex/SourceLocation.h"
#include "Operation.h"
#include "Expr.h"
#include "Decl.h"
#include "RecursiveASTVisitor.h"
#include "AST_EXPORTS.h"


namespace internal
{
  // Stmt ids [it is needed for our RTTI]
  enum StmtId
  {
    ExpressionStmtId,
    DeclarationStmtId,
    IfStmtId,
    WhileStmtId,
    DoStmtId,
    ForStmtId,
    CompoundStmtId,
    BreakId,
    ContinueId
  };
}


class AST_EXPORT Stmt
{
public:
  Stmt(internal::StmtId id) :
      m_id(id)
  {
  }
  Stmt(SourceLocation loc, internal::StmtId id) :
      m_location(loc), m_id(id)
  {
  }
  virtual ~Stmt() {}

  static bool classof(Stmt const *) { return true; }
  internal::StmtId getId() const { return m_id; }

  SourceLocation GetLocation() { return m_location; }
  void SetLocation(SourceLocation loc) { m_location = loc; }

  virtual std::string ToString(int ident = 0) = 0;
  virtual void Accept(RecursiveASTVisitor *visitor) = 0;

private:
  internal::StmtId m_id;
  SourceLocation m_location;
};


// ExpressionStmt
//
//
class AST_EXPORT ExpressionStmt : public Stmt
{
public:
  ExpressionStmt();
  ExpressionStmt(Expr *expr, SourceLocation loc);
  ~ExpressionStmt();

  static bool classof(ExpressionStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::ExpressionStmtId; }

  Expr *GetExpr();
  void SetExpr(Expr *expr);

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_expr;
};


// DeclarationStmt
//
//
class AST_EXPORT DeclarationStmt : public Stmt
{
public:
  DeclarationStmt();
  DeclarationStmt(Decl *decl, SourceLocation loc);
  ~DeclarationStmt();

  static bool classof(DeclarationStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::DeclarationStmtId; }

  Decl *GetDecl();
  void SetDecl(Decl *decl);

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Decl *m_decl;
};


// IfStmt
//
//
class AST_EXPORT IfStmt : public Stmt
{
public:
  IfStmt();
  IfStmt(Expr *cond,
         Stmt *then,
         Stmt *otherwise,
         SourceLocation loc);
  ~IfStmt();

  static bool classof(IfStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::IfStmtId; }

  Expr *GetCond();
  void SetCond(Expr *cond);
  Stmt *GetThen();
  void SetThen(Stmt *then);
  Stmt *GetElse();
  void SetElse(Stmt *otherwise);

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_cond;
  Stmt *m_then;
  Stmt *m_else;
};


// WhileStmt
//
// Node that represents a while statement
class AST_EXPORT WhileStmt : public Stmt
{
public:
  WhileStmt();
  WhileStmt(Expr *cond,
            Stmt *body,
            SourceLocation loc);
  ~WhileStmt();

  static bool classof(WhileStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::WhileStmtId; }

  Expr *GetCond();
  void SetCond(Expr *cond);
  Stmt *GetBody();
  void SetBody(Stmt *body);

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_cond;
  Stmt *m_body;
};


// DoStmt
//
// Node that represents do-while stmt
class AST_EXPORT DoStmt : public Stmt
{
public:
  DoStmt();
  DoStmt(Expr *cond,
         Stmt *body,
         SourceLocation loc);
  ~DoStmt();

  static bool classof(DoStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::DoStmtId; }

  Expr *GetCond();
  void SetCond(Expr *cond);
  Stmt *GetBody();
  void SetBody(Stmt *body);

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Expr *m_cond;
  Stmt *m_body;
};


// ForStmt
//
// Node that represents for statement
class AST_EXPORT ForStmt : public Stmt
{
public:
  ForStmt();
  ForStmt(Stmt *init, 
          Expr *cond, 
          Expr *step,
          Stmt *body,
          SourceLocation loc);
  ~ForStmt();

  static bool classof(ForStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::ForStmtId; }

  Stmt *GetInit();
  void SetInit(Stmt *init);
  Expr *GetCond();
  void SetCond(Expr *cond);
  Expr *GetStep();
  void SetStep(Expr *step);
  Stmt *GetBody();
  void SetBody(Stmt *body);

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  Stmt *m_init;
  Expr *m_cond;
  Expr *m_step;
  Stmt *m_body;
};


// CompoundStmt
//
//
class AST_EXPORT CompoundStmt : public Stmt
{
public:
  CompoundStmt();
  CompoundStmt(SourceLocation loc);
  ~CompoundStmt();

  static bool classof(CompoundStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::CompoundStmtId; }

  void Add(Stmt *stmt);
  int Count();
  std::list<Stmt *>::iterator Begin();
  std::list<Stmt *>::iterator End();

  std::string ToString(int ident = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;

private:
  std::list<Stmt *> m_stmts;
};


// BreakStmt
//
//
class AST_EXPORT BreakStmt : public Stmt
{
public:
  BreakStmt();
  BreakStmt(SourceLocation loc);
  ~BreakStmt();

  static bool classof(BreakStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::BreakId; }

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;
};


// ContinueStmt
//
//
class AST_EXPORT ContinueStmt : public Stmt
{
public:
  ContinueStmt();
  ContinueStmt(SourceLocation loc);
  ~ContinueStmt();

  static bool classof(ContinueStmt const *) { return true; }
  static bool classof(Stmt const *s) { return s->getId() == internal::ContinueId; }

  std::string ToString(int indent = 0) override;
  void Accept(RecursiveASTVisitor *visitor) override;
};

#endif /* STMT_H */