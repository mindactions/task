#ifndef SEMANTIC_ANALYZER_H
#define SEMANTIC_ANALYZER_H

#include <string>
#include "Lex/DiagnosticsEngine.h"
#include "RecursiveASTVisitor.h"
#include "Scope.h"
#include "AST_EXPORTS.h"
#include "Stmt.h"


class AST_EXPORT SemanticAnalyzer : public RecursiveASTVisitor
{
public:
  SemanticAnalyzer(ASTContext *context);
  ~SemanticAnalyzer();

  bool Analyze();

  bool VisitIfStmt(IfStmt *stmt) override;
  bool VisitWhileStmt(WhileStmt *stmt) override;
  bool VisitDoStmt(DoStmt *stmt) override;
  bool VisitForStmt(ForStmt *stmt) override;
  bool VisitBreakStmt(BreakStmt *) override;
  bool VisitContinueStmt(ContinueStmt *) override;

  bool VisitVarExpr(VarExpr *expr) override;
  bool VisitNumberExpr(NumberExpr *expr) override;
  bool VisitBinOpExpr(BinOpExpr *expr) override;
  bool VisitUnaryExpr(UnaryExpr *expr) override;
  bool VisitConsExpr(ConsExpr *expr) override;

  bool VisitVarDecl(VarDecl *decl) override;

private:
  void Error(SourceLocation loc, std::string message);

private:
  DiagnosticsEngine &m_diag;
  ScopeStack m_scope;
  int m_cycleCount;
};

#endif /* SEMANTIC_ANALYZER_H */