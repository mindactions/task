#ifndef AST_VISITOR_H
#define AST_VISITOR_H

#include "ASTContext.h"
#include "Common/Object.h"
#include "AST_EXPORTS.h"

//
// NOTE: forward declaration is used to break
//       cyclic dependency
//
class Stmt;
class Expr;
class Decl;
class VarExpr;
class NumberExpr;
class BinOpExpr;
class UnaryExpr;
class ConsExpr;
class IfStmt;
class WhileStmt;
class DoStmt;
class ForStmt;
class BreakStmt;
class ContinueStmt;
class CompoundStmt;
class ExpressionStmt;
class DeclarationStmt;
class VarDecl;
class CompoundDecl;


class AST_EXPORT RecursiveASTVisitor
{
public:
  RecursiveASTVisitor(ASTContext *content) :
      m_context(content)
  {
  }
  virtual ~RecursiveASTVisitor() {}

  void Traverse();

  virtual void BeforeTraverse() {}
  virtual void AfterTraverse() {}
  virtual void BeforeStmt(Stmt *) {}
  virtual void AfterStmt(Stmt *) {}
  virtual void BeforeExpr(Expr *) {}
  virtual void AfterExpr(Expr *) {}
  virtual void BeforeDecl(Decl *) {}
  virtual void AfterDecl(Decl *) {}

  virtual bool VisitExpr(Expr *expr);
  virtual bool VisitStmt(Stmt *stmt);
  virtual bool VisitDecl(Decl *decl);

  virtual bool VisitVarExpr(VarExpr *expr) { return true; }
  virtual bool VisitNumberExpr(NumberExpr *expr) { return true; }
  virtual bool VisitBinOpExpr(BinOpExpr *expr) { return true; }
  virtual bool VisitUnaryExpr(UnaryExpr *expr) { return true; }
  virtual bool VisitConsExpr(ConsExpr *expr) { return true; }

  virtual bool VisitIfStmt(IfStmt *stmt) { return true; }
  virtual bool VisitWhileStmt(WhileStmt *stmt) { return true; }
  virtual bool VisitDoStmt(DoStmt *stmt) { return true; }
  virtual bool VisitForStmt(ForStmt *stmt) { return true; }
  virtual bool VisitBreakStmt(BreakStmt *stmt) { return true; }
  virtual bool VisitContinueStmt(ContinueStmt *stmt) { return true; }
  virtual bool VisitExpressionStmt(ExpressionStmt *stmt) { return true; }
  virtual bool VisitDeclarationStmt(DeclarationStmt *stmt) { return true; }
  virtual bool VisitCompoundStmt(CompoundStmt *stmt) { return true; }

  virtual bool VisitVarDecl(VarDecl *decl) { return true; }
  virtual bool VisitCompoundDecl(CompoundDecl *decl) { return true; }

private:
  ASTContext *m_context;
};

#endif /* AST_VISITOR_H */