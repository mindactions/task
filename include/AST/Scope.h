#ifndef SCOPE_H
#define SCOPE_H

#include <map>
#include <list>
#include <string>
#include "AST_EXPORTS.h"


class AST_EXPORT Scope
{
public:
  Scope();
  ~Scope();

  // Check if scope has variable
  //   \param name: variable name
  //   \return: true if has, false otherwise
  bool Has(std::string name);

  // Get value of a variable
  //   \param name: variable name
  //   \param defaultValue: default value(if there is no such variable)
  //   \return: value of the variable
  int Get(std::string name, int defaultValue = 0);

  // Set value of the variable
  //   \param name: name of the variable
  //   \param value: new value of the variable
  void Set(std::string name, int value);

  // TODO: remove this
  int &operator[](std::string name);

private:
  std::map<std::string, int> m_variables;
};


class AST_EXPORT ScopeStack
{
public:
  ScopeStack();
  ~ScopeStack();

  // Check if there are any scopes
  bool Empty();

  // Create new scope
  void Enter();

  // Delete most closest scope
  void Leave();

  // Get reference to the current scope
  Scope &Current();

  // Check if scope stack has variable
  //   \param name: variable name
  //   \return: true if has, false otherwise
  bool Has(std::string name);

  // Get value of a variable
  //   \param name: variable name
  //   \param defaultValue: default value(if there is no such variable)
  //   \return: value of the variable
  int Get(std::string name, int defaultValue = 0);

  // Set value of the variable
  //   \param name: name of the variable
  //   \param value: new value of the variable
  void Set(std::string name, int value);

  void Declare(std::string name, int value = 0);

private:
  std::list<Scope> m_scopes;
};

#endif /* SCOPE_H */