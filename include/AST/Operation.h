#ifndef OPERATION_H
#define OPERATION_H

#include "Lex/Token.h"
#include "AST_EXPORTS.h"


enum class BinOp
{
  NONE,                     // unknown
  ADD,                      // +
  SUB,                      // -
  DIV,                      // /
  MUL,                      // *
  MOV,                      // =
  LT,                       // <
  LTE,                      // <=
  GT,                       // >
  GTE,                      // >=
  EQ,                       // ==
  NE,                       // !=
  LOGICAL_OR,               // ||
  LOGICAL_AND,              // &&
  COMMA                     // ,
};


enum class UnaryOp
{
  NONE,                    // unknown
  INC,                     // ++
  DEC,                     // --
  NOT                      // !
};


namespace Precendence
{
  enum Level
  {
    Unknown         = 0,
    Comma           = 1,  // ,
    Assignment      = 2,  // =
    Conditional     = 3,  // TODO: ?
    LogicalOr       = 4,  // ||
    LogicalAnd      = 5,  // &&
    InclusiveOr     = 6,  // TODO: |
    ExculsiveOr     = 7,  // TODO: ^
    And             = 8,  // TODO: &
    Equality        = 9,  // ==, !=
    Relational      = 10, // <, >
    Shift           = 11, // TODO: <<, >>
    Additive        = 12, // +, -
    Multiplicative  = 13, // *, /
    End             = -1, // EOF
  };
}


// Get precendence of a binary operator
//   \param token: binary op token
//   \return: precendence
Precendence::Level AST_EXPORT GetBinOpPrecendence(Token token);

// Convert token to BinOp
//   \param token: input token
//   \return: BinOp
BinOp AST_EXPORT GetBinOpForToken(Token token);

#endif /* OPERATION_H */