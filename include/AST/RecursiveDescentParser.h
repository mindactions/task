#ifndef RECURSIVE_DESCENT_PARSER_H
#define RECURSIVE_DESCENT_PARSER_H

#include "Lex/Lexer.h"
#include "Lex/Token.h"
#include "Lex/DiagnosticsEngine.h"
#include "Expr.h"
#include "Stmt.h"
#include "Operation.h"
#include "AST_EXPORTS.h"
#include <memory>


namespace internal
{
  template<typename T>
  class Result
  {
  public:
    Result() : m_value(nullptr) {}
    Result(T *value) : m_value(value) {}
    bool IsValid() { return m_value != 0; }
    bool IsInvalid() { return m_value == 0; }
    void Invalidate()
    {
      if (m_value)
      {
       delete m_value;
       m_value = nullptr;
     }
    }
    T* Get() { return m_value; }

  private:
    T *m_value;
  };
}


class AST_EXPORT RecursiveDescentParser
{
public:
  RecursiveDescentParser(Lexer *lexer, DiagnosticsEngine *diag);

  internal::Result<Stmt> Parse();

private:

  Token NextToken();
  Token ConsumeToken();
  Token CurrentToken();
  Token LookAhead(int k);
  int SaveTokenIndex();
  void RestoreTokenIndex(int index);
  void DoesNotExpect(TokenKind kind);
  void Expect(TokenKind kind);
  bool ExpectSafe(TokenKind kind);
  void Error(std::string message);

private:

  //
  // Parse expressions
  //
  internal::Result<Expr> ParseExpression(bool followCommas = true);
  internal::Result<Expr> ParseAssignmentExpression();
  internal::Result<Expr> ParseRHSOfBinaryExpression(internal::Result<Expr> lhs, Precendence::Level minPrecendence);
  internal::Result<Expr> ParseConditionalExpression();
  internal::Result<Expr> ParseLogicalORExpression();
  internal::Result<Expr> ParseLogicalANDExpression();
  internal::Result<Expr> ParseEqualityExpression();
  internal::Result<Expr> ParseRelationalExpression();
  internal::Result<Expr> ParseAdditiveExpression();
  internal::Result<Expr> ParseMultiplicativeExpression();
  internal::Result<Expr> ParseUnaryExpression();
  internal::Result<Expr> ParsePostfixExpression();
  internal::Result<Expr> ParsePrimary();

  //
  // Parse statements
  //
  internal::Result<Stmt> ParseStatements();
  internal::Result<Stmt> ParseStatement();
  internal::Result<Stmt> ParseExpressionStatement();
  internal::Result<Stmt> ParseDeclarationStatement();
  internal::Result<Stmt> ParseSelectionStatement();
  internal::Result<Stmt> ParseIterationStatement();
  internal::Result<Stmt> ParseForInitStatement();
  internal::Result<Stmt> ParseJumpStatement();
  internal::Result<Stmt> ParseCompoundStatement();
  internal::Result<Stmt> ParseStatementList();

  //
  // Parse declarations
  //
  internal::Result<Decl> ParseDeclaration();
  internal::Result<Decl> ParseDeclaratorListDeclaration();
  internal::Result<Decl> ParseDeclaratorDeclaration();
  internal::Result<Expr> ParseInitializeDeclaration();

private:
  Lexer *m_lexer;
  DiagnosticsEngine *m_diag;
  Token m_token;
};

#endif /* RECURSIVE_DESCENT_PARSER_H */