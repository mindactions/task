#ifndef AST_CONTEXT_H
#define AST_CONTEXT_H

#include <string>
#include "Lex/DiagnosticsEngine.h"
#include "AST_EXPORTS.h"


// 
// NOTE: forward declaration is used to break cyclic dependency:
//  RecursiveASTVisitor - ASTContext - Stmt/Expr
//
class Stmt;
class RecursiveDescentParser;


class AST_EXPORT ASTContext
{
public:
  ASTContext(RecursiveDescentParser *parser,
             DiagnosticsEngine *diag);
  ~ASTContext();

  // Get DiagnosticsEngine
  //   \return: DiagnosticsEngine
  DiagnosticsEngine &GetDiag();

  // Print AST to string
  //   \return: "converted" AST
  std::string GetASTString();

  // Get root node of the AST
  //   \return: root Stmt
  Stmt *GetRoot();

private:
  Stmt *m_root;
  DiagnosticsEngine *m_diag;
};

#endif /* AST_CONTEXT_H */