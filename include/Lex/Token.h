#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "SourceLocation.h"


enum class TokenKind
{
  WHILE,              // while
  DO,                 // do
  FOR,                // for
  BREAK,              // break
  IF,                 // if
  ELSE,               // else
  CONTINUE,           // continue
  IDENTIFIER,         // variableName
  NUMBER,             // 123
  LEFT_PARENTHESIS,   // (
  RIGHT_PARENTHESIS,  // )
  LEFT_BRACE,         // {
  RIGHT_BRACE,        // }
  SEMICOLON,          // ;
  COMMA,              // ,
  LT,                 // <
  LTE,                // <=
  GT,                 // >
  GTE,                // >=
  EQ,                 // ==
  NE,                 // !=
  NOT,                // !
  EQUAL,              // =
  INC,                // ++
  PLUS,               // +
  DEC,                // --
  MINUS,              // -
  MUL,                // *
  DIV,                // /
  LOGICAL_OR,         // &&
  LOGICAL_AND,        // ||
  INT,                // int
  END                 // EOF
};


//
// Token structure
//
//  m_kind : kind of a token
//  m_location : location of the token
//  m_length : length of the token
//  m_payload : --
//
struct Token
{
  Token(TokenKind kind = TokenKind::END,
        SourceLocation loc = SourceLocation(-1, -1),
        int length = -1,
        std::string payload = "") :
      m_kind(kind),
      m_location(loc),
      m_length(length),
      m_payload(payload)
  {
  }

  TokenKind m_kind;
  SourceLocation m_location;
  int m_length;
  std::string m_payload;
};


//
// TokenKindToString()
//
//   Convert TokenKind to std::string
//
inline std::string
TokenKindToString(TokenKind kind)
{
  switch (kind)
  {
    case TokenKind::WHILE:
      return "while";
    case TokenKind::DO:
      return "do";
    case TokenKind::FOR:
      return "for";
    case TokenKind::BREAK:
      return "break";
    case TokenKind::IF:
      return "if";
    case TokenKind::ELSE:
      return "else";
    case TokenKind::CONTINUE:
      return "continue";
    case TokenKind::LEFT_PARENTHESIS:
      return "(";
    case TokenKind::RIGHT_PARENTHESIS:
      return ")";
    case TokenKind::LEFT_BRACE:
      return "{";
    case TokenKind::RIGHT_BRACE:
      return "}";
    case TokenKind::SEMICOLON:
      return ";";
    case TokenKind::COMMA:
      return ",";
    case TokenKind::LT:
      return "<";
    case TokenKind::LTE:
      return "<=";
    case TokenKind::GT:
      return ">";
    case TokenKind::GTE:
      return ">=";
    case TokenKind::EQ:
      return "==";
    case TokenKind::NE:
      return "!=";
    case TokenKind::NOT:
      return "!";
    case TokenKind::EQUAL:
      return "=";
    case TokenKind::INC:
      return "++";
    case TokenKind::PLUS:
      return "+";
    case TokenKind::DEC:
      return "--";
    case TokenKind::MINUS:
      return "-";
    case TokenKind::MUL:
      return "*";
    case TokenKind::DIV:
      return "/";
    case TokenKind::LOGICAL_OR:
      return "||";
    case TokenKind::LOGICAL_AND:
      return "&&";
    case TokenKind::INT:
      return "int";
    default:
      return "";
  }
}

#endif /* TOKEN_H */