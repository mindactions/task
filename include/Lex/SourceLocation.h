#ifndef SOURCE_LOCATION_H
#define SOURCE_LOCATION_H

#include <ostream>


struct SourceLocation
{
  SourceLocation(int line = -1, int column = -1) :
      m_line(line), m_column(column)
  {
  }

  int m_line;
  int m_column;
};


inline std::ostream &
operator << (std::ostream &out, const SourceLocation &loc)
{
  out << loc.m_line + 1 << ":" << loc.m_column + 1;
  return out;
}

#endif /* SOURCE_LOCATION_H */