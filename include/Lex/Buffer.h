#ifndef BUFFER_H
#define BUFFER_H

#include <string>

class Buffer{
public:
  Buffer( std::string text ){ m_text = text; }
  std::string getData() const {
    return m_text;
  }
private:
  std::string m_text;
};

#endif /* BUFFER_H */
