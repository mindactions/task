#ifndef SOURCE_MANAGER_H
#define SOURCE_MANAGER_H

#include <string>
#include <vector>
#include "SourceLocation.h"
#include "Lex_EXPORTS.h"
#include "Buffer.h"


class LEX_EXPORT SourceManager
{
public:
  SourceManager();

  // Load from file
  SourceManager(std::string filename);
  SourceManager(const Buffer & buffer);
  ~SourceManager();

  // Go at the beginning of the file
  void Reset();

  // Get next line
  //   \return: line, or throws exception if EOF was reached
  std::string GetNextLine();

  // Check if has next line
  //   \returns true - has, false otherwise
  bool HasNextLine();

  // Get substring
  //   \param loc: location
  //   \param length: size of the substring
  //   \return: string or throws exception
  std::string At(SourceLocation &loc, int length);

  // Get filename
  //   \return: filename
  std::string GetFilename();

  // Get total lines
  //   \return: total number of lines in the file
  int GetTotalLines();

  // Get line
  //   \return: line with index lineNo
  std::string GetLine(int lineNo);

private:
  int m_currentLine;
  std::string m_filename;
  std::vector<std::string> m_lines;
};

#endif /* SOURCE_MANAGER_H */
