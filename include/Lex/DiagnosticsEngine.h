#ifndef DIAGNOSTICS_ENGINE_H
#define DIAGNOSTICS_ENGINE_H

#include <string>
#include <iostream>
#include "SourceManager.h"
#include "SourceLocation.h"
#include "Lex_EXPORTS.h"

#ifdef _WIN32 || _WIN64
#include <Windows.h>
#endif


namespace colors
{
#ifdef __linux__
  inline std::ostream &
  black(std::ostream &s)
  {
    s << "\033[1;30m";
    return s;
  }

  inline std::ostream &
  red(std::ostream &s)
  {
    s << "\033[1;31m";
    return s;
  }

  inline std::ostream &
  green(std::ostream &s)
  {
    s << "\033[1;32m";
    return s;
  }

  inline std::ostream &
  yellow(std::ostream &s)
  {
    s << "\033[1;33m";
    return s;
  }

  inline std::ostream &
  blue(std::ostream &s)
  {
    s << "\033[1;34m";
    return s;
  }

  inline std::ostream &
  magenta(std::ostream &s)
  {
    s << "\033[1;35m";
    return s;
  }

  inline std::ostream &
  cyan(std::ostream &s)
  {
    s << "\033[1;36m";
    return s;
  }

  inline std::ostream &
  white(std::ostream &s)
  {
    s << "\033[1;37m";
    return s;
  }

  inline std::ostream &
  reset(std::ostream &s)
  {
    s << "\033[0m";
    return s;
  }
#endif

#ifdef _WIN32 || _WIN64
  inline std::ostream &
  black(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, 0);
    return s;
  }

  inline std::ostream &
  red(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_RED|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  green(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  yellow(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  blue(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  magenta(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  cyan(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  white(std::ostream &s)
  {
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
    return s;
  }

  inline std::ostream &
  reset(std::ostream &s)
  {
    return white(s);
  }
#endif
}


class LEX_EXPORT DiagnosticsEngine
{
public:
  DiagnosticsEngine(SourceManager *sm);
  ~DiagnosticsEngine();

  // Print note message
  //   \param message: message to print out
  static void Note(std::string message);

  // Print note message and note it in the code
  //   \param loc: location
  //   \param message: note to print out
  void Note(SourceLocation loc, std::string message);

  // Print warning message
  //   \param message: message
  static void Warning(std::string message);

  // Print warning message and note it in the code
  //   \param loc: location
  //   \param message: message
  void Warning(SourceLocation loc, std::string message);

  // Print error message
  //   \param message: message to print out
  static void Error(std::string message);

  // Print error message and note it in the code
  //   \param loc: location
  //   \param message: message
  void Error(SourceLocation loc, std::string message);

private:
  SourceManager *m_sm;
};

#endif /* DIAGNOSTICS_ENGINE_H */
