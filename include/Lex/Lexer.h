#ifndef LEXER_H
#define LEXER_H

#include "SourceManager.h"
#include "DiagnosticsEngine.h"
#include "Token.h"
#include "Lex_EXPORTS.h"
#include <vector>


class LEX_EXPORT Lexer
{
public:
  Lexer(SourceManager *sm);
  Lexer(SourceManager *sm, DiagnosticsEngine *diag);
  ~Lexer();

  // Go to the first token
  void Reset();

  // Check if it has next token
  //   \return: true has, false otherwise
  bool HasNextToken();

  // Get next token
  //   \return: next token, if EOF was reached token
  //            kind is END
  Token GetNextToken();

  // Lookahead
  //   \param at: how much
  //   \return: token on success, throws exception otherwise
  Token Lookup(int at);

  // Get token with given index
  //   \param index: index of the token
  //   \return: token on success, throws expreception otherwise
  Token At(int index);

  // Get source manager
  //   \return: source manager reference
  SourceManager& GetSourceManager();

  // Get index of the current token
  //   \return: index
  int GetCurrentTokenIndex();

  // Set index of the current token
  //   \param index: index
  void SetCurrentTokenIndex(int index);

private:
  Token GetEndToken();
  void Tokenize();
  void TokenizeLine(std::string &line, int lineNo);
  void Error(SourceLocation loc, std::string message);

private:
  SourceManager *m_sourceManager;
  DiagnosticsEngine *m_diag;
  std::vector<Token> m_tokens;
  int m_currentToken;
};

#endif /* LEXER_H */