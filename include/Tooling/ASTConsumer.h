#ifndef AST_CONSUMER_H
#define AST_CONSUMER_H

#include "Lex/SourceManager.h"
#include "Lex/DiagnosticsEngine.h"
#include "AST/ASTContext.h"
#include "AST/RecursiveASTVisitor.h"
#include "Tooling/Options.h"
#include "Tooling_EXPORTS.h"


class TOOLING_EXPORT ASTConsumer
{
public:
  ASTConsumer();
  virtual ~ASTConsumer();

  Options &GetOptions();
  void SetOptions(Options *options);
  void BuildAST(SourceManager *sm, DiagnosticsEngine *diag);
  virtual void BeforeStart() {}
  void Start();
  virtual void AfterStart() {}

  virtual void RunASTConsumer(ASTContext &ctx) = 0;

private:
  ASTContext *m_astContext;
  Options *m_options;
};

#endif /* AST_CONSUMER_H */