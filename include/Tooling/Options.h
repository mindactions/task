#ifndef _OPTIONS_H
#define _OPTIONS_H

#include <string>
#include <vector>
#include "Tooling_EXPORTS.h"


class TOOLING_EXPORT Options
{
public:
  Options(int argc, char **argv);
  ~Options();

  std::vector<std::string> GetSourceFiles();
  bool HasAnySourceFiles();
  bool HasOption(std::string option);

private:
  void parseOptions(int argc, char **argv);

private:
  std::vector<std::string> m_sourceFiles;
  std::vector<std::string> m_options;
};

#endif /* _OPTIONS_H */