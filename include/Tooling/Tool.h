#ifndef TOOL_H
#define TOOL_H

#include <memory>
#include "Lex/SourceManager.h"
#include "Lex/DiagnosticsEngine.h"
#include "Tooling/Options.h"
#include "ASTConsumer.h"
#include "Tooling_EXPORTS.h"


class TOOLING_EXPORT Tool
{
public:
  Tool(int argc, char **argv);
  ~Tool();

  int run(std::unique_ptr<ASTConsumer> consumer);

private:
  SourceManager *m_sourceManager;
  DiagnosticsEngine *m_diag;
  Options *m_options;
};

#endif /* TOOL_H */