cmake_minimum_required(VERSION 3.0)
project(Cyclux) # TODO: rename

if (UNIX)
  add_compile_options(-std=c++11)
endif (UNIX)

#
# custom functions
#
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
include(AddCycluxTest)
include(AddCycluxTool)

#
# generate shared exports
# windows
#
include(GenerateExportHeader)
include_directories(${CMAKE_BINARY_DIR}/exports)

#
# set output directories
#
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

#
# build gtest
#
add_subdirectory(third_party)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})

#
# build framework
#
include_directories(${CMAKE_SOURCE_DIR}/include)
add_subdirectory(lib)
add_subdirectory(tools)

#
# build tests
#
enable_testing()
add_subdirectory(tests)
