# Tools

## ast-dump

```sh
$ bin/ast-dump file.cpp
```


## cyclux-v1

```sh
$ bin/cyclux-v1 file.cpp

# or

$ bin/cyclux-v1 --pretty file.cpp
```